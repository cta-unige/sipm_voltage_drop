#include <TH1D.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TString.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include "TChain.h"
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TCut.h>
#include <TF1.h>
#include <TF2.h>
#include <TH1.h>
#include <TMath.h>
#include <TRandom3.h>
#include <TRandom1.h>
/*#include "Fit/Fitter.h"
#include "Fit/Bindata_5.h"*/
#include "HFitInterface.h"
#include "TRandom.h"
#include "TSpline.h"
#include "TGaxis.h"
#include "./input_data/spline.h"

//C, C++
#include <stdio.h>
//#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>
#include <sstream>
#include <ctime>

using namespace std;

// Set Global Parameters:
Double_t el = 1.60217662e-19;
Double_t OvervoltageInitial;
Int_t nWaveformSim;
Int_t nIterationsPX = 0;
Int_t nMaxPxt = 500;
Int_t nPulses = 0;
Int_t nXTPulses = 0;
Double_t IntegralRange[2];// = {300., 800.};
Double_t Cucell;// = 85.e-15;//85.e-15;//38.e-15;//85.e-15;//85.e-15;
Double_t Rbias;// = 10.e3;//10.e3;
Double_t Rq;// = 190.e3;//249.4e3;
Double_t Ncell;// = 4.*9210;
Double_t Vbd;// = 54.7654;//54.699;
Double_t Vbias;// = Vbd + 3.22;//2.97; //2.8;
Double_t AmpliSlope;// = /*0.000698199;/*/0.00042812;
Double_t OverShootRatio;
Double_t Gfadc;// = 1.;//0.000313;
Double_t SigmaElectronic;// = 0.0;//1.254;
Double_t ElectronicBaseine;
Double_t SigmaGain;// = 0.0;//0.0918; //persent 1 p.e.
Double_t StartTime;// = 0.0;
Double_t EndTime;// = 1.e-6;
Double_t SensorArea;// = 93.6;
bool fixedWavelength;// = false;
bool useWindow;// = false;
Double_t SimWavelength;// = 468.;
Double_t NormFactor;// = 1.;//2.5;//2.5;
bool DCR_On;// = true;
bool Pxt_On;// = true;
bool PDE_On;// = true;
bool Pap_On;// = true;
bool doInteger; // = false
Double_t PulseTime;// = 500.e-9;
bool SimACPulse;// = true;
Double_t nPEPulse;// = 100.;
Double_t nPhotonsPulse;// = 2.*55.;
Double_t LightPulseWavelength;// = 468.;//468.;
bool usePhotons;// = true;
bool CompensationLoop;// = false;
TString templateToRead;// = "template_SST_20MHz.txt";//*"TemplateOutYves.txt";//"template_R50Ohm.txt";//"TemplateACDC.txt";/*/"template_SST1M.txt";
Double_t Sampling;// = 1.e-9;
Double_t TemplateSampling;
TString DetectorName;
Double_t PapTau = -6.30904e+07;
Double_t TemplateTau = 10.e-9;

struct MyTemplate{
    
    // Structure to store the template parameters

    Int_t NofPoints;	        // number of points in the template
    Double_t Amplitude[25000];	//array of amplitude values, [V]
    Double_t Time[25000];
    Double_t ChargeToNorm;
    Double_t Tau;               //time constant, ns
    Double_t RiseTime = 16.e-9; //time constant, ns
    Double_t Charge5tau;        //5tau charg
    Double_t Charge4tau;        //5tau charg
    Double_t Charge3tau;        //5tau charg
    Double_t Charge2tau;        //5tau charg
    Double_t Charge1tau;        //5tau charg
    Int_t Temperature;          //Tempearature
    Int_t Position;             //Template minimum position, by default = 100
    Double_t ChargeFall = 0.0;
    Double_t Gain;
    Double_t Current[1000];
    Double_t CurrentTime[1000];
    Int_t nPointsCurrent;
    Int_t nPointsSpline;
    Double_t TimeSpline[10000];
    Double_t AmpliSpline[10000];
    Int_t OneTemplateStep;
    Int_t nTemplateUsfullPoints;
    Double_t ChargeToNormSpline;


};

struct MySimParameters{
    
    // Structure to store the simulation parameters

  Double_t AverageCurrent;
  Double_t AverageOvervoltage;
  Double_t AverageGain;
  Double_t AveragePDE;
  Double_t AverageADC;
  Double_t AveragePxt;
  Double_t AverageDCR;
  Double_t SimfNSB;
  Double_t CalcNSB;
  Int_t nPhotonSimAverage;
  Int_t nPhotonSimReal;
  Int_t nPEDetected;
  Int_t nPEDetectedACPulse;
  Double_t TimeWindow;
  Double_t SimfNSBReal;
  Double_t SimfNSBRealPE;
  Double_t WavelengthSimulated[100000];
  Double_t WavelengthPEDetected[100000];
  Double_t nPhotonDetected;
  Double_t nPhotonRecalculated;
  Double_t AveragePDEforNSB;
  Double_t PDEtoNSBReal;
  Double_t PDENSBEff;
  Double_t fNSBEfficiency;
  Double_t nPEEfficiency;
  Double_t BaseLine;
  Double_t VoltaDrop;
  Double_t AdditionalBaseline;

  Int_t Npoints = 10000;
  Double_t Time[10000];
  Double_t Increment;
  Double_t Current[10000];
  Double_t CurrentVsTimeGenerated[10000];
  Double_t CurrentVsTimeDetected[10000];
  Double_t CurrentVsTimeDetectedAverage;
  Double_t VbiasVsTime[10000];
  Double_t Npe[10000];
  Double_t ADCAmpli[10000];
  Double_t Overvoltage[10000];
  Double_t OvervoltageSimpleModel[10000];
  Double_t PxtData[10000];
  Double_t PDESignalData[10000];
  Double_t GainData[10000];
  Double_t AmplitudeData[10000];
  Double_t DCRData[10000];
  Double_t WaveformPEGenerated[10000];
  Double_t WaveformPEDetected[10000];
  Double_t WaveformAmpliDetected[10000];
  Double_t WaveformAmpliDetectedInverted[10000];


};

struct MyNSB{
    
  // Structure to store the NSB parameters

  Double_t Wavelength[1000]; // Wavelength;
  Double_t Intensity[1000];	// 10^9 / (nm s m^2 sr)
  Double_t IntensityNormToWindow[1000];	// 10^9 / (nm s m^2 sr)
  Double_t IntensityNormToWindowSum[1000];
  Double_t Integral;
  Int_t nPoint;
};

struct MyData{
    
  // Structure to store the simulated data

  //TH1D *hGeneratedNSB = new TH1D("hGeneratedNSB", "NSB wavelegth, nm", 1200., 0.0, 1200);
  TH1D *h1CurrentAverage;
  TH1D *h1ADCBaseline;
  TH1D *h1Overvoltage;
  TH1D *h1Pxt;
  TH1D *h1PDE;
  TH1D *h1Gain;
  TH1D *h1ADC;
  TH1D *h1DCR;
  TH1D *h1NSBWavelength;

  TGraph *grSimWaveformPE;
  TGraph *grDetectedWaveformPE;
  TGraph *grSimWaveformADC;
  TGraph *grDetectedWaveformADC;

  TGraph *grCurrentVsTime;
  TGraph *grBaselineADCVsTime;
  TGraph *grOvervoltageVsTime;
  TGraph *grPxtVsTime;
  TGraph *grPDE470nmVsTime;
  TGraph *grGainVsTime;
  TGraph *gr1peAmpliVsTime;
  TGraph *grDCRVsTime;



};

struct MyWindow{
    
  // Structure to store camera Window parameters

  Int_t nPoint;
  Double_t Wavelength[1000];
  Double_t Transparency[1000];
  Double_t WavelengthErr[1000];
  Double_t TransparencyErr[1000];
};

// this part related to data representation:
void SetTransparentPadBottom(TVirtualPad *overlay); // to create transparent Pad at Bottom of canvas
void SetTopFrame(TVirtualPad *pad1, TString Xtytle, TString Ytytle, bool logyscale, Double_t *XRange, Double_t *YRange); // to create transparent Pad at Top of canvas
void SetAxisToDraw(TGaxis *axis, TString title);    // to create Axis to Draw

// Get SiPM pulse template from file and set it into Template structure:
void SetTemplate(TString path, MyTemplate *mytemplate);
void SetTemplateTxt(TString path, MyTemplate *mytemplate, TString path2);


void FindAmpliGain(MyTemplate *mytemplate); // not used

// Add SiPM pulse in a waveform:
void AddPulse(Double_t *Y, Double_t *YDetected, Double_t *Current, Double_t *X, Int_t nP, MyTemplate *mytemplate, Double_t mytiem, Double_t Pxt, Double_t peAmplitude, Double_t OverVoltage);
// Add afterpulse in a waveform:
void AddAfterpulse(Double_t *Y, Double_t *YDetected, Double_t *Current, Double_t *X, Int_t nP, MyTemplate *mytemplate, Double_t mytiem, Double_t Pxt, Double_t peAmplitude, Double_t Overvoltage);

// Generate waveform with a given parameters:
void DoCalParamVsTime(MyNSB *NSB, Double_t fNSB, Double_t *WindowRange, MyTemplate *mytemplate, TF2 *f2, Int_t MyColor, bool drawAxis, bool doAverage, MySimParameters *OutParameters);

void ReadNSB(TString path,  MyNSB *NSB);            // Read NSB parameters from input file and set it into NSB structure
void ReadWindow(TString path,  MyWindow *Window);   // Read Camera Window (i.e. Borofloat) parameters from input file and set it into Window structure
void CalcIntegralNSB(MyNSB *NSB, Double_t *Range);  // Integrate NSB

// Simulate NSB photon wavelength:
void doFindPhotonWavelength(MyNSB *NSB, Double_t *Range, Double_t *FoundWavelength);           // -> not used
void doFindPhotonWavelengthCorrected(MyNSB *NSB, Double_t *Range, Double_t *FoundWavelength);  // -> used


void DoCalcCorrectedCharge(MyTemplate *mytemplate, Double_t *Correction);  // Calculate Template integral
void Solve2Equat(Double_t a, Double_t b, Double_t c, Double_t *Result);    // solve a + b*x + c*x*x equation
void MySmooth(Double_t *array, Double_t *SmoothArray, Int_t NomberOfEllements, Int_t HalfOfSumm); // Do smoothing

void readInputParameters(TString InFile); // read input parametrs from the input file
void calNSB(Double_t simNSB); // -> Main program, to run simulation

Double_t fGeiger(Double_t *x, Double_t *par)
{
   // function to describe Geiger probability vs. Vbias, wher par[1] is a Vbd
   Double_t fitval = 1. - TMath::Exp(-par[0]*(x[0]-par[1]));
   return fitval;
}

Double_t Ampli1peADC(Double_t OverVoltage){

  // function to describe 1 p.e. Amplitude vs. Overvoltage
  // Gfadc -> Gain of fast ADC
  //Double_t fitval = 0.00056151*(OverVoltage)/(Gfadc);
  Double_t fitval = AmpliSlope*(OverVoltage)/(Gfadc);
  return fitval;
}

Double_t fPDE(Double_t *x, Double_t *par)
{
   // function to describe PDE (at a given wavelength) vs. Overvoltage
   Double_t fitval = par[1]*(1. - TMath::Exp(-par[0]*(x[0])));
   return fitval;
}


Double_t Gain(Double_t Overvoltage){

  // SiPM Gain vs. Overvoltage
  Double_t G = Cucell*Overvoltage/el;
  return G;// = 1.5e6;
}

Double_t GainDerivative(Double_t Overvoltage){

  // SiPM Gain derivative
  Double_t dG = Cucell/el;
  return dG;// = 1.5e6;
}

Double_t DCRRate(Double_t Overvoltage, Double_t Area){
    
  // DCR as a function of Overvoltage and Area
  // parameters fixed her are for LCT5 Hex Sensor
  Double_t par[3];
  par[0] = 3.73183e-01;
  par[1] = 2.90142e+04;
  par[2] = 1.21878e-01;

  Double_t DCR;

  if(DCR_On){
    DCR = Area*par[1]*(TMath::Exp(par[2]*Overvoltage))*(1. - TMath::Exp(-par[0]*Overvoltage)) ;
  }
  else{
    //DCR = Area*par[1]*(TMath::Exp(par[2]*2.8))*(1. - TMath::Exp(-par[0]*2.8)) ;
    DCR = 0.0;
  }
  return DCR;
}

Double_t Pxt(Double_t Overvoltage){

  // Pxt as a function of Overvoltage
  // parameters fixed her are for LCT5 Hex Sensor
  Double_t par1 = /*9.;/*/7.22578;
  Double_t par0 = 0.139822;

  Double_t OpticalCrosstalk;
  if(Pxt_On){
    OpticalCrosstalk = 0.01*par1*Overvoltage*(1 - TMath::Exp(-par0*Overvoltage) );
  }
  else{
    //OpticalCrosstalk = 0.01*par1*2.8*(1 - TMath::Exp(-par0*2.8) );
    OpticalCrosstalk = 0.0;
  }
  return OpticalCrosstalk;
}

Double_t Pap(Double_t Overvoltage){
    
  // Afterpulses probability as a function of Overvoltage and Area
  // parameters fixed her are for LCT5 Hex Sensor

  Double_t par0 = 0.0;
  Double_t par1 = 0.0;//2.64067e-03;
  Double_t par2 = 0.0038303;//1.76974e-02;

  Double_t Afterpulses;
  if(Pap_On){
    Afterpulses = par0 + par1*Overvoltage + par2*Overvoltage*Overvoltage;
  }
  else{
    Afterpulses = 0.0;
  }
  return Afterpulses;
}

Double_t PDEMax(Double_t *x, Double_t *par){

  // PDE vs. wavelength at a given overvoltage
  // x - wavelegth, *par should be given for a given Overvoltage
  Double_t fitval;
  if((x[0]<=370)&&(x[0]>=260)){
     fitval = par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
  }
  else if( (x[0]<=530)&&(x[0]>=370) ){
     fitval = par[3] + par[4]*x[0] + par[5]*x[0]*x[0];
  }
  else if( (x[0]<800)&&(x[0]>=530) ){
     fitval = par[6] + par[7]*x[0] + par[8]*x[0]*x[0];
  }
  else if( (x[0]<1000)&&(x[0]>=800) ){
     fitval = par[9] + par[10]*x[0] + par[11]*x[0]*x[0];
  }
  return fitval;
}

Double_t PDESlope(Double_t *x, Double_t *par){
   // describe PDE slope with Overvoltage
   Double_t fitval;
   fitval = par[0] + par[1]*x[0] + par[2]*x[0]*x[0]; //+ par[3]*x[0]*x[0]*x[0];
   return fitval;
}

Double_t fun2(Double_t *x, Double_t *par) {

   // x[0] = wavelegth;
   // x[1] = Overvoltage;
   //PDEFit = PDEMax - the PDE at saturation;
   //PDESlope =

   Double_t *p1 = &par[0];
   Double_t *p2 = &par[12];
   Double_t result = PDEMax(&x[0], p1)*(1 - TMath::Exp( -PDESlope(&x[0], p2)*x[1] ) );
   if(!PDE_On){
     result = 1.;
     //result = PDEMax(&x[0], p1)*(1 - TMath::Exp( -PDESlope(&x[0], p2)*2.8 ) );
   }
   return result;
}

Double_t fun2PE(Double_t *x, Double_t *par) {

   // x[0] = wavelegth;
   // x[1] = Overvoltage;
   //PDEFit = PDEMax - the PDE at saturation;
   //PDESlope =

   Double_t *p1 = &par[0];
   Double_t *p2 = &par[12];

   /*par[0] = -1.29693e+00;
   par[1] = 8.27659e-03;
   par[2] = -1.02035e-05;
   par[3] = -1.89539e+00;
   par[4] = 9.78309e-03;
   par[5] = -9.97473e-06;
   par[6] = 1.25334e+00;
   par[7] = -1.53717e-03;
   par[8] = 1.96296e-07;
   par[9] = 2.09973e+00;
   par[10] = -3.90231e-03;
   par[11] = 1.82918e-06;
   par[12] = 4.96106e-01;
   par[13] = -4.65596e-05;
   par[14] = -3.03291e-07};*/

   Double_t result = PDEMax(&x[0], p1)*(1 - TMath::Exp( -PDESlope(&x[0], p2)*x[1] ) );
   return result;
}


int main(int argc, char *argv[]){

  if(argc == 4){
    Double_t simNSB = atof(argv[1]);
    cout<<"f NSB : "<<simNSB<<endl;
    nWaveformSim = atoi(argv[2]);
    cout<<"n Waveforms : "<<nWaveformSim<<endl;

    readInputParameters(argv[3]);

    calNSB(simNSB);
  }
  else{
    cout<<"--------------------------------------------------"<<endl;
    cout<<"   Error : 'Wrong number of input parameters :'"<<endl;
    cout<<"parm. (1) : NSB in gamma/s"<<endl;
    cout<<"parm. (2) : number of waveforms to Simulate."<<endl;
    cout<<"parm. (3) : input file with parameters to Simulate"<<endl;
    cout<<"--------------------------------------------------"<<endl;
  }
}

void readInputParameters(TString InFile) {

  ifstream inputpar;
  inputpar.open(InFile);
  assert(inputpar.is_open());
  TString parname;

  TString charTmp;

  cout<<"----------------------------------------"<<endl;
  cout<<"Read Input Parameters"<<endl;
  cout<<endl;
  while ( !inputpar.eof() ){
     inputpar>>parname;
     if(parname == "DetectorName:"){
        cout<<"ParName : "<<parname;
        inputpar>>DetectorName;
        cout<<" "<<DetectorName<<endl;
     }
     else if(parname == "Cucell_fF:"){
        cout<<"ParName : "<<parname;
        inputpar>>Cucell;
        Cucell = Cucell*1.e-15;
        cout<<" "<<Cucell<<endl;
     }
     else if(parname == "Rbias_K:"){
        cout<<"ParName : "<<parname;
        inputpar>>Rbias;
        Rbias = Rbias*1.e3;
        cout<<" "<<Rbias<<endl;
     }
     else if(parname == "Rquenching_K:"){
        cout<<"ParName : "<<parname;
        inputpar>>Rq;
        Rq = Rq*1.e3;
        cout<<" "<<Rq<<endl;
     }
     else if(parname == "Ncells:"){
        cout<<"ParName : "<<parname;
        inputpar>>Ncell;
        cout<<" "<<Ncell<<endl;
     }
     else if(parname == "Vbd_V:"){
        cout<<"ParName : "<<parname;
        inputpar>>Vbd;
        cout<<" "<<Vbd<<endl;
     }
     else if(parname == "Vbias:"){
        cout<<"ParName : "<<parname;
        inputpar>>Vbias;
        cout<<" "<<Vbias<<endl;
     }
     else if(parname == "AmpliSlope:"){
        cout<<"ParName : "<<parname;
        inputpar>>AmpliSlope;
        cout<<" "<<AmpliSlope<<endl;
     }
     else if(parname == "OverShootRatio:"){
        cout<<"ParName : "<<parname;
        inputpar>>OverShootRatio;
        cout<<" "<<OverShootRatio<<endl;
     }
     else if(parname == "Gfadc:"){
        cout<<"ParName : "<<parname;
        inputpar>>Gfadc;
        cout<<" "<<Gfadc<<endl;
     }
     else if(parname == "SigmaElect:"){
        cout<<"ParName : "<<parname;
        inputpar>>SigmaElectronic;
        cout<<" "<<SigmaElectronic<<endl;
     }
     else if(parname == "ElectronicBaseine:"){
        cout<<"ParName : "<<parname;
        inputpar>>ElectronicBaseine;
        cout<<" "<<ElectronicBaseine<<endl;
     }
     else if(parname == "SigmaGain:"){
        cout<<"ParName : "<<parname;
        inputpar>>SigmaGain;
        cout<<" "<<SigmaGain<<endl;
     }
     else if(parname == "SimStartTime:"){
        cout<<"ParName : "<<parname;
        inputpar>>StartTime;
        cout<<" "<<StartTime<<endl;
     }
     else if(parname == "SimEndTime:"){
        cout<<"ParName : "<<parname;
        inputpar>>EndTime;
        cout<<" "<<EndTime<<endl;
     }
     else if(parname == "SensorArea:"){
        cout<<"ParName : "<<parname;
        inputpar>>SensorArea;
        cout<<" "<<SensorArea<<endl;
     }
     else if(parname == "UseFixedWavelength:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          fixedWavelength = true;
        }
        else if (charTmp == "false"){
          fixedWavelength = false;
        }
        else{
          cout<<"Error : 'UseFixedWavelength' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<fixedWavelength<<endl;
     }
     else if(parname == "SimWavelength_nm:"){
        cout<<"ParName : "<<parname;
        inputpar>>SimWavelength;
        cout<<" "<<SimWavelength<<endl;
     }
     else if(parname == "UseWindow:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          useWindow = true;
        }
        else if (charTmp == "false"){
          useWindow = false;
        }
        else{
          cout<<"Error : 'useWindow' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<useWindow<<endl;
     }
     else if(parname == "SetWavelength_Range:"){
       cout<<"ParName : "<<parname;
       inputpar>>IntegralRange[0];
       inputpar>>IntegralRange[1];
       cout<<" "<<IntegralRange[0]<<" "<<IntegralRange[1]<<endl;
     }
     else if(parname == "CurrentNormFactor:"){
        cout<<"ParName : "<<parname;
        inputpar>>NormFactor;
        cout<<" "<<NormFactor<<endl;
     }
     else if(parname == "DoInteger:"){
       cout<<"ParName : "<<parname;
       inputpar>>charTmp;
       if(charTmp == "true"){
         doInteger = true;
       }
       else if (charTmp == "false"){
         doInteger = false;
       }
       else{
         cout<<"Error : 'DoInteger' should be 'true' or 'false' "<<endl;
       }
       cout<<" "<<doInteger<<endl;
     }
     else if(parname == "UseDCR:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          DCR_On = true;
        }
        else if (charTmp == "false"){
          DCR_On = false;
        }
        else{
          cout<<"Error : 'UseDCR' should be 'true' or 'false' "<<endl;
        }

        cout<<" "<<DCR_On<<endl;
     }
     else if(parname == "UsePxt:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          Pxt_On = true;
        }
        else if (charTmp == "false"){
          Pxt_On = false;
        }
        else{
          cout<<"Error : 'UsePxt' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<Pxt_On<<endl;
     }
     else if(parname == "UsePDE:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          PDE_On = true;
        }
        else if (charTmp == "false"){
          PDE_On = false;
        }
        else{
          cout<<"Error : 'UsePDE' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<PDE_On<<endl;
     }
     else if(parname == "UsePap:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          Pap_On = true;
        }
        else if (charTmp == "false"){
          Pap_On = false;
        }
        else{
          cout<<"Error : 'UsePDE' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<Pap_On<<endl;
     }
     else if(parname == "SimACPulse:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          SimACPulse = true;
        }
        else if (charTmp == "false"){
          SimACPulse = false;
        }
        else{
          cout<<"Error : 'SimACPulse' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<SimACPulse<<endl;
     }
     else if(parname == "ACPulseTime:"){
        cout<<"ParName : "<<parname;
        inputpar>>PulseTime;
        cout<<" "<<PulseTime<<endl;
     }
     else if(parname == "nPEInACPulse:"){
        cout<<"ParName : "<<parname;
        inputpar>>nPEPulse;
        cout<<" "<<nPEPulse<<endl;
     }
     else if(parname == "nPhotonsInACPulse:"){
        cout<<"ParName : "<<parname;
        inputpar>>nPhotonsPulse;
        cout<<" "<<nPhotonsPulse<<endl;
     }
     else if(parname == "ACPulseWavelength_nm:"){
        cout<<"ParName : "<<parname;
        inputpar>>LightPulseWavelength;
        cout<<" "<<LightPulseWavelength<<endl;
     }
     else if(parname == "UsePhotonsInACPulse:"){
       cout<<"ParName : "<<parname;
       inputpar>>charTmp;
        if(charTmp == "true"){
          usePhotons = true;
        }
        else if (charTmp == "false"){
          usePhotons = false;
        }
        else{
          cout<<"Error : 'UsePhotonsInACPulse' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<usePhotons<<endl;
     }
     else if(parname == "Template:"){
        cout<<"ParName : "<<parname;
        inputpar>>templateToRead;
        cout<<" "<<templateToRead<<endl;
     }
     else if(parname == "SamplingTime:"){
        cout<<"ParName : "<<parname;
        inputpar>>Sampling;
        cout<<" "<<Sampling<<endl;
     }
     else if(parname == "TemplateSampling:"){
        cout<<"ParName : "<<parname;
        inputpar>>TemplateSampling;
        cout<<" "<<TemplateSampling<<endl;
     }
     else if(parname == "UseCompensationLoop:"){
        cout<<"ParName : "<<parname;
        inputpar>>charTmp;
        if(charTmp == "true"){
          CompensationLoop = true;
        }
        else if (charTmp == "false"){
          CompensationLoop = false;
        }
        else{
          cout<<"Error : 'UseCompensationLoop' should be 'true' or 'false' "<<endl;
        }
        cout<<" "<<CompensationLoop<<endl;
     }
  }
  //myconditions->LeftSide = myconditions->LeftSide/myconditions->XIncrement;

  OvervoltageInitial = Vbias - Vbd;

  inputpar.close();
  cout<<endl;
  cout<<"----------------------------------------"<<endl;
  cout<<endl;

}

void calNSB(Double_t simNSB){

  //Set Template
  MyTemplate Template;
  SetTemplateTxt(templateToRead, &Template, "SiPM_noAP_LCT2_Current.txt");

  //Set PDE
  const Int_t npar = 15;
  Double_t f2params[npar] =
     {-1.29693e+00, 8.27659e-03, -1.02035e-05, -1.89539e+00, 9.78309e-03, -9.97473e-06, 1.25334e+00, -1.53717e-03, 1.96296e-07,
      2.09973e+00, -3.90231e-03, 1.82918e-06, 4.96106e-01, -4.65596e-05, -3.03291e-07};
   TF2 *f2 = new TF2("f2",fun2, 260, 650, 0.2, 7., npar);
   f2->SetParameters(f2params);


  //Set NSB Spectrum
  MyNSB NSB;
  ReadNSB("/Users/nagai/Documents/work/DoAna/DoCalcNSB/WithSignal/V3/NSBStady/Spectra_NSB_halfmoon.txt",  &NSB);

  //Set Window
  MyWindow Window;
  ReadWindow("/Users/nagai/Documents/work/DoAna/PreparePlots/window/WindowTransparancyCalculated.txt",  &Window);

  //Interpolate Window
  Int_t npWindow = Window.nPoint-1;
  std::vector<double> XWindow(npWindow), YWindow(npWindow);
  for(int i=0; i<npWindow; i++){
    XWindow[i] = Window.Wavelength[i];
    YWindow[i] = Window.Transparency[i];
  }
  tk::spline sWindow;
  sWindow.set_points(XWindow, YWindow);

  if(!fixedWavelength){
    for(Int_t point=0; point<NSB.nPoint; point++){
      if(useWindow){
          NSB.IntensityNormToWindow[point] = NSB.Intensity[point]*sWindow(NSB.Wavelength[point]);
      }
      else{
        NSB.IntensityNormToWindow[point] = NSB.Intensity[point];
      }
    }
  }
  else{
    if(useWindow){
      NSB.IntensityNormToWindow[0] = NSB.Intensity[0]*sWindow(SimWavelength);
    }
    else{
      NSB.IntensityNormToWindow[0] = NSB.Intensity[0];
    }
  }

  if(useWindow){
    nPhotonsPulse = nPhotonsPulse*sWindow(LightPulseWavelength);
    cout<<"N photon changed to : "<<nPhotonsPulse;
  }

  CalcIntegralNSB(&NSB, IntegralRange);


 MySimParameters OutParameters;

 //Create Out file name
 std::ostringstream strs, strsNSB;
 strs << Vbias - Vbd;
 strsNSB << simNSB;
 std::string strOverV = strs.str();
 std::string strNSB = strsNSB.str();
 TString TOverV = strOverV;
 TString TNSB = strNSB + "_Hz";
 TString Compensation;
 if(CompensationLoop){
   Compensation = "_Compenstion_On";
 }
 else{
   Compensation = "_Compenstion_Off";
 }
 TString FileName = TOverV + "V_" + TNSB;
 FileName = FileName + Compensation + ".root";
 cout<<"File Name : "<<FileName<<endl;



 TFile *FileOut = new TFile(FileName, "RECREATE", "Some Waveform", 1);
 if (FileOut->IsZombie()) {
   exit(-1);
 }

 TTree *TreeOut = new TTree("A", "Waveform Data (V)");
 TreeOut->SetAutoSave(1000000);
 TTree::SetBranchStyle(0);

 TreeOut-> Branch("nPhotonsInPulse",&nPhotonsPulse,"nPhotonsPulse/D");
 TreeOut-> Branch("nNSBPhotons",&OutParameters.nPhotonSimReal,"nPhotonSimReal/I");
 TreeOut-> Branch("TimeWindow",&OutParameters.TimeWindow,"TimeWindow/D");
 TreeOut-> Branch("NSBinPhotons",&OutParameters.SimfNSBReal,"SimfNSBReal/D");
 TreeOut-> Branch("NSBinPE",&OutParameters.SimfNSBRealPE,"SimfNSBRealPE/D");
 TreeOut-> Branch("nPEDetectedNSB",&OutParameters.nPEDetected,"nPEDetected/I");
 TreeOut-> Branch("nPEDetectedInPulse",&OutParameters.nPEDetectedACPulse,"nPEDetectedACPulse/I");
 TreeOut-> Branch("RealPDEforNSB",&OutParameters.PDEtoNSBReal,"PDEtoNSBReal/D");
 TreeOut-> Branch("WavelengthNSBPEDetected",OutParameters.WavelengthPEDetected,"WavelengthPEDetected[nPEDetected]/D");
 TreeOut-> Branch("WavelengthPulsedLight", &LightPulseWavelength,"LightPulseWavelength/D");
 TreeOut-> Branch("WavelengthNSBSimulated",OutParameters.WavelengthSimulated,"WavelengthSimulated[nPhotonSimReal]/D");
 TreeOut-> Branch("NpointsInWaveform",&OutParameters.Npoints,"Npoints/I");
 TreeOut-> Branch("Time", OutParameters.Time,"Time[Npoints]/D");
 TreeOut-> Branch("WaveformPEGenerated",OutParameters.WaveformPEGenerated,"WaveformPEGenerated[Npoints]/D");
 TreeOut-> Branch("WaveformPEDetected",OutParameters.WaveformPEDetected,"WaveformPEDetected[Npoints]/D");
 TreeOut-> Branch("WaveformAmpliDetected",OutParameters.WaveformAmpliDetected,"WaveformAmpliDetected[Npoints]/D");
 TreeOut-> Branch("DC_Baseline", &OutParameters.AdditionalBaseline, "AdditionalBaseline/D");
 TreeOut-> Branch("Current",OutParameters.Current,"Current[Npoints]/D");
 TreeOut-> Branch("CurrentVsTimeGenerated",OutParameters.CurrentVsTimeGenerated,"CurrentVsTimeGenerated[Npoints]/D");
 TreeOut-> Branch("CurrentVsTimeDetected",OutParameters.CurrentVsTimeDetected,"CurrentVsTimeDetected[Npoints]/D");
 TreeOut-> Branch("VbiasVsTime",OutParameters.VbiasVsTime,"VbiasVsTime[Npoints]/D");
 TreeOut-> Branch("Npe",OutParameters.Npe,"Npe[Npoints]/D");
 TreeOut-> Branch("Overvoltage",OutParameters.Overvoltage,"Overvoltage[Npoints]/D");
 TreeOut-> Branch("OvervoltageSimpleModel",OutParameters.OvervoltageSimpleModel,"OvervoltageSimpleModel[Npoints]/D");
 TreeOut-> Branch("PxtData",OutParameters.PxtData,"PxtData[Npoints]/D");
 TreeOut-> Branch("PDESignalData",OutParameters.PDESignalData,"PDESignalData[Npoints]/D");
 TreeOut-> Branch("GainData",OutParameters.GainData,"GainData[Npoints]/D");
 TreeOut-> Branch("AmplitudeData",OutParameters.AmplitudeData,"AmplitudeData[Npoints]/D");
 TreeOut-> Branch("DCRData",OutParameters.DCRData,"DCRData[Npoints]/D");

 TreeOut-> Branch("NpointsInNSBSpectrum",&NSB.nPoint,"nPoint/I");
 TreeOut-> Branch("NSBIntensity", NSB.Intensity,"Intensity[nPoint]/D");
 TreeOut-> Branch("NSBWavelength",NSB.Wavelength,"Wavelength[nPoint]/D");
 TreeOut-> Branch("NSBIntensityNormToWindow", NSB.IntensityNormToWindow,"IntensityNormToWindow[nPoint]/D");

 TreeOut-> Branch("NpointsInWindowTransparency",&Window.nPoint,"nPointWD/I");
 TreeOut-> Branch("WindowTransparency", Window.Transparency,"Transparency[nPointWD]/D");
 TreeOut-> Branch("WindowTransparencyErr", Window.TransparencyErr,"TransparencyErr[nPointWD]/D");
 TreeOut-> Branch("WindowWavelength",Window.Wavelength,"Wavelength[nPointWD]/D");
 TreeOut-> Branch("WindowWavelengthErr",Window.WavelengthErr,"WavelengthErr[nPointWD]/D");

 TreeOut-> Branch("Cucell",&Cucell,"Cucell/D");
 TreeOut-> Branch("Rq",&Rq,"Rq/D");
 TreeOut-> Branch("Rbias",&Rbias,"Rbias/D");
 TreeOut-> Branch("Ncell",&Ncell,"Ncell/D");
 TreeOut-> Branch("Vbias",&Vbias,"Vbias/D");
 TreeOut-> Branch("Vbd",&Vbd,"Vbd/D");
 TreeOut-> Branch("SigmaElectronic",&SigmaElectronic,"SigmaElectronic/D");
 TreeOut-> Branch("ElectronicBaseine",&ElectronicBaseine,"ElectronicBaseine/D");
 TreeOut-> Branch("SigmaGain",&SigmaGain,"SigmaGain/D");
 TreeOut-> Branch("OverShootRatio",&OverShootRatio,"OverShootRatio/D");

 Double_t InitialADCAmpli = Ampli1peADC(Vbias - Vbd);
 TreeOut-> Branch("Ampli1peADCInitial",&InitialADCAmpli,"InitialADCAmpli/D");
 TreeOut-> Branch("SensorArea",&SensorArea,"SensorArea/D");
 TreeOut-> Branch("NormFactor",&NormFactor,"NormFactor/D");

 TTree *TreeOutDataFormat = new TTree("T", "Waveform Data (V)");
 TreeOutDataFormat->SetAutoSave(1000000);
 TTree::SetBranchStyle(0);
 TreeOutDataFormat-> Branch("NPoints",&OutParameters.Npoints,"Npoints/I");
 TreeOutDataFormat-> Branch("V1", OutParameters.WaveformAmpliDetectedInverted, "WaveformAmpliDetectedInverted[Npoints]/D");
 TreeOutDataFormat-> Branch("dt",&OutParameters.Increment,"Increment/D");
 TreeOutDataFormat-> Branch("Vbias",&Vbias,"Vbias/D");
 TreeOutDataFormat-> Branch("Ibias",&OutParameters.CurrentVsTimeDetectedAverage,"CurrentVsTimeDetectedAverage/D");

 for(int i=0; i<nWaveformSim; i++){
    if(i%1000 == 0){
      cout<<"Start Waveform # "<<i<<"\t"<<"Over : "<<nWaveformSim<<endl;
    }

    DoCalParamVsTime(&NSB, simNSB, IntegralRange, &Template, f2, 1, true, false, &OutParameters);

    OutParameters.TimeWindow = (EndTime - StartTime);
    OutParameters.nPEDetected = OutParameters.nPEDetected;
    OutParameters.nPEDetectedACPulse = OutParameters.nPEDetectedACPulse;
    OutParameters.nPhotonSimReal = OutParameters.nPhotonSimReal;
    OutParameters.SimfNSBReal = (OutParameters.nPhotonSimReal)/(OutParameters.TimeWindow);
    OutParameters.SimfNSBRealPE = (OutParameters.nPEDetected)/(OutParameters.TimeWindow);

    TreeOut->Fill();
    TreeOutDataFormat->Fill();
 }


  FileOut->Write();
  FileOut->Close();

  cout<<"N pulses :" <<nPulses<<"; n PXT :"<<nXTPulses<<endl;

}

void ReadNSB(TString path,  MyNSB *NSB){

  TString ch1;
  Int_t n = 0;

   if(!fixedWavelength){
     ifstream indata;
     indata.open(path);
     assert(indata.is_open());

     while ( !indata.eof() ){
        indata>>NSB->Wavelength[n];
        //indata>>ch1;
        indata>>NSB->Intensity[n];
        NSB->Intensity[n] = (NSB->Intensity[n])*1.e9;
        n++;
      }
   }
   else{
     NSB->Wavelength[n] = SimWavelength;
     NSB->Intensity[n] = 1.;
     n++;
   }
   NSB->nPoint = n;
}

void CalcIntegralNSB(MyNSB *NSB, Double_t *Range) {
  NSB->Integral = 0.0;
  cout<<"Range : "<<Range[0]<<" "<<Range[1]<<endl;
  for(int point=0; point<NSB->nPoint-1; point++){
    if((NSB->Wavelength[point]>=Range[0])&&(NSB->Wavelength[point]<=Range[1])){
       NSB->Integral = NSB->Integral + 0.5*(NSB->IntensityNormToWindow[point] + NSB->IntensityNormToWindow[point+1])*(NSB->Wavelength[point+1] - NSB->Wavelength[point]);
       //cout<<(NSB->IntensityNormToWindow[point] + NSB->IntensityNormToWindow[point+1])*(NSB->Wavelength[point+1] - NSB->Wavelength[point])<<endl;
    }
  }

  //NSB->Integral = NSB->Integral*1.e9;
}

void ReadWindow(TString path,  MyWindow *Window){
  ifstream indata;
  indata.open(path);
  assert(indata.is_open());

  TString ch1;
  Int_t n = 0;

  while ( !indata.eof() ){
     indata>>Window->Wavelength[n];
     indata>>Window->Transparency[n];
     indata>>Window->WavelengthErr[n];
     indata>>Window->TransparencyErr[n];
     if(!useWindow){
       Window->Transparency[n] = 1.;
       Window->TransparencyErr[n] = 0.0;
     }
     n++;
   }
   Window->nPoint = n;
}

void DoCalParamVsTime(MyNSB *NSB, Double_t fNSB, Double_t *WindowRange, MyTemplate *mytemplate, TF2 *f2, Int_t MyColor, bool drawAxis, bool doAverage, MySimParameters *OutParameters){

  Double_t TimeStep = Sampling;
  Int_t nTimeIterations = (EndTime - StartTime)/TimeStep;

  Double_t *Waveform = new Double_t[nTimeIterations];
  Double_t *WaveformDetected = new Double_t[nTimeIterations];
  Double_t *WaveformAmpli = new Double_t[nTimeIterations];
  Double_t *WaveformDetectedAmpli = new Double_t[nTimeIterations];
  Double_t *Wtime = new Double_t[nTimeIterations];
  Double_t *CurrentVsTime = new Double_t[nTimeIterations];
  Double_t *CurrentVsTimeGenerated = new Double_t[nTimeIterations];
  Double_t *CurrentVsTimeDetected = new Double_t[nTimeIterations];
  Double_t *CurrentVsTimeSimple = new Double_t[nTimeIterations];
  Double_t *VbiasVsTime = new Double_t[nTimeIterations];
  Double_t *OvervoltageVsTime = new Double_t[nTimeIterations];
  Double_t *OvervoltageSimpleVsTime = new Double_t[nTimeIterations];
  Double_t *PxtVsTime = new Double_t[nTimeIterations];
  Double_t *DCRVsTime = new Double_t[nTimeIterations];
  Double_t *PDEVsTime = new Double_t[nTimeIterations];
  Double_t *GainVsTime = new Double_t[nTimeIterations];
  Double_t *ADCVsTime = new Double_t[nTimeIterations];
  Double_t *NpeVsTime = new Double_t[nTimeIterations];

  TRandom3 *r1Elecl = new TRandom3();
  r1Elecl->SetSeed(0);
  for(int itime=0; itime<nTimeIterations; itime++){
      Waveform[itime] = 0.0;
      //WaveformAmpli[itime] = ElectronicBaseine;//r1Elecl->Gaus(ElectronicBaseine, SigmaElectronic*ElectronicBaseine);;
      WaveformAmpli[itime] = r1Elecl->Gaus(ElectronicBaseine, SigmaElectronic*ElectronicBaseine);;
      WaveformDetected[itime] = 0.0;
      //WaveformDetectedAmpli[itime] = ElectronicBaseine;//r1Elecl->Gaus(ElectronicBaseine, SigmaElectronic*Ampli1peADC(Vbias-Vbd));
      WaveformDetectedAmpli[itime] = r1Elecl->Gaus(ElectronicBaseine, SigmaElectronic*Ampli1peADC(Vbias-Vbd));
      CurrentVsTime[itime] = 0.0;
      CurrentVsTimeGenerated[itime] = 0.0;
      CurrentVsTimeDetected[itime] = 0.0;
      CurrentVsTimeSimple[itime] = 0.0;
      NpeVsTime[itime] = 0.0;
      ADCVsTime[itime] = 0.0;
      DCRVsTime[itime] = 0.0;
      VbiasVsTime[itime] = Vbias;

      Wtime[itime] = StartTime + itime*TimeStep;
  }

  Double_t Overvoltage, OvervoltageSimple;
  TRandom *r0 = new TRandom();

  //fNsDistance = 1.9e-9;
  Double_t fNsDistance = 1./fNSB;
  Double_t NSBPulseProbability = Sampling/fNsDistance;
  Double_t DCRProbability = 0.0;
  Double_t GainInitial = Gain(Vbias - Vbd);

  TRandom3 *r1 = new TRandom3();
  r1->SetSeed(0);

  TRandom3 *r1PDE = new TRandom3();
  r1PDE->SetSeed(0);

  TRandom3 *r1DCR = new TRandom3();
  r1DCR->SetSeed(0);

  TRandom3 *r1Pap = new TRandom3();
  r1Pap->SetSeed(0);

  Int_t nPhotonConst = 0;
  if(NSBPulseProbability>=1.) {
    nPhotonConst = (int) NSBPulseProbability;
    NSBPulseProbability = NSBPulseProbability - nPhotonConst;
    //cout<<"nPhotonConst : "<<nPhotonConst<<" NSBPulseProbability : "<<NSBPulseProbability<<endl;
  }

  Double_t rPulse, rPDE, rDCR;
  Int_t nPhotonSimulated=10000;
  Double_t *PhotonWavelength = new Double_t[nPhotonSimulated];
  Double_t *xPhoton = new Double_t[nPhotonSimulated];
  nPhotonSimulated = 0;
  OutParameters->nPEDetected = 0;
  OutParameters->nPEDetectedACPulse = 0;
  OutParameters->nPhotonSimReal = 0;
  CurrentVsTime[0] = 0.0;
  CurrentVsTimeSimple[0] = 0.0;
  NpeVsTime[0] = 0.0;


  Double_t AveragePDESimple = 0.0;
  Int_t nAveragePDESimple = 0;
  Int_t nPEpulsesToSim;
  Double_t nPEPulseWithDrop;

  const Int_t npar = 15;

  Double_t f2params[npar] =
     {-1.29693e+00, 8.27659e-03, -1.02035e-05, -1.89539e+00, 9.78309e-03, -9.97473e-06, 1.25334e+00, -1.53717e-03, 1.96296e-07,
      2.09973e+00, -3.90231e-03, 1.82918e-06, 4.96106e-01, -4.65596e-05, -3.03291e-07};

   TF2 *f2Drop = new TF2("f2Drop", fun2PE, 260, 650, 0.2, 7., npar);
   f2Drop->SetParameters(f2params);

////////

    r1->SetSeed(0);
    r1PDE->SetSeed(0);
    r1DCR->SetSeed(0);
    r1Pap->SetSeed(0);

    Overvoltage = VbiasVsTime[0] - Vbd;
    OvervoltageSimple = VbiasVsTime[0]-Vbd;
    nIterationsPX = 0;
    CurrentVsTime[0] = 0.0;
    CurrentVsTimeDetected[0] = 0.0;
    CurrentVsTimeGenerated[0] = 0.0;
    CurrentVsTimeSimple[0] = 0.0;
    NpeVsTime[0] = 0.0;
    ADCVsTime[0] = Ampli1peADC(Vbias - Vbd);

    for(int itime=0; itime<nTimeIterations; itime++){
      rPulse = r1->Uniform(1.);
      rDCR = r1DCR->Uniform(1.);
      if(itime>0){
        Overvoltage = VbiasVsTime[itime-1] - Vbd - Rbias*CurrentVsTimeDetected[itime-1] - (Rq*CurrentVsTimeDetected[itime-1]*NpeVsTime[itime-1])/Ncell;
        OvervoltageSimple = VbiasVsTime[itime-1] - Vbd - Rbias*CurrentVsTimeDetected[itime-1];

        if( Rbias*CurrentVsTimeDetected[itime-1] > VbiasVsTime[itime-1] - Vbd){
          Overvoltage = 0.0;
          if(itime==0) cout<<"Error : Overvoltage < 0.0, Overvoltage == 0.0 V"<<endl;
        }
        if(Rbias*CurrentVsTime[itime-1] > VbiasVsTime[itime-1] - Vbd){
          OvervoltageSimple = 0.0;
        }
      }
      else {
        Overvoltage = VbiasVsTime[0] - Vbd;
        OvervoltageSimple = VbiasVsTime[0] - Vbd;
      }
      OvervoltageVsTime[itime] = Overvoltage;
      OvervoltageSimpleVsTime[itime] = OvervoltageSimple;
      PxtVsTime[itime] = Pxt(Overvoltage);
      for(int nPulse=0; nPulse<nPhotonConst; nPulse++){
        // add Constant NSB rate
        rPDE = r1PDE->Uniform(1.);
        //cout<<rPDE<<" ";
        nIterationsPX = 0;
        AddPulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
          AddAfterpulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        }
        nIterationsPX = 0;
        nIterationsPX = 0;
        doFindPhotonWavelengthCorrected(NSB, WindowRange, &PhotonWavelength[nPhotonSimulated]);
        OutParameters->WavelengthSimulated[nPhotonSimulated] = PhotonWavelength[nPhotonSimulated];
        OutParameters->nPhotonSimReal++;
        //cout<<PhotonWavelength[nPhotonSimulated]<<endl;
        AveragePDESimple = AveragePDESimple + f2->Eval(PhotonWavelength[nPhotonSimulated], Overvoltage);
        nAveragePDESimple++;
        //if(Wtime[itime] > 300.e-9) hWavelengthCut->Fill(PhotonWavelength[nPhotonSimulated]);
        xPhoton[nPhotonSimulated] = nPhotonSimulated;
        nPhotonSimulated++;
        if(f2->Eval(PhotonWavelength[nPhotonSimulated-1], Overvoltage) >= rPDE) {
          nIterationsPX = 0;
          OutParameters->WavelengthPEDetected[OutParameters->nPEDetected] = PhotonWavelength[nPhotonSimulated-1];
          OutParameters->nPEDetected++;
          AddPulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
          if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
            AddAfterpulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
          }
          NpeVsTime[itime] = NpeVsTime[itime] + 1. + nIterationsPX;
          nIterationsPX = 0;
        }
      }
      DCRVsTime[itime] = DCRRate(Overvoltage, SensorArea);
      DCRProbability = DCRVsTime[itime]*Sampling;
      if(DCRProbability >= rDCR){
        // add DCR
        nIterationsPX = 0;
        AddPulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
          AddAfterpulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        }
        AddPulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
        if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
          AddAfterpulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
        }
        NpeVsTime[itime] = NpeVsTime[itime] + 1. + nIterationsPX;
        nIterationsPX = 0;
      }
      if( NSBPulseProbability >= rPulse){
        // add NSB
        nIterationsPX = 0;
        AddPulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
          AddAfterpulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, 1., Vbias - Vbd);
        }
        nIterationsPX = 0;
        nIterationsPX = 0;
        doFindPhotonWavelengthCorrected(NSB, WindowRange, &PhotonWavelength[nPhotonSimulated]);
        OutParameters->WavelengthSimulated[nPhotonSimulated] = PhotonWavelength[nPhotonSimulated];
        OutParameters->nPhotonSimReal++;
        //cout<<PhotonWavelength[nPhotonSimulated]<<endl;
        AveragePDESimple = AveragePDESimple + f2->Eval(PhotonWavelength[nPhotonSimulated], Overvoltage);
        nAveragePDESimple++;
        //if(Wtime[itime] > 300.e-9) hWavelengthCut->Fill(PhotonWavelength[nPhotonSimulated]);
        xPhoton[nPhotonSimulated] = nPhotonSimulated;
        nPhotonSimulated++;
        rPDE = r1PDE->Uniform(1.);
        //cout<<rPDE<<endl;
        if(f2->Eval(PhotonWavelength[nPhotonSimulated-1], Overvoltage) >= rPDE) {
          // add Detected Pulse
          nIterationsPX = 0;
          OutParameters->WavelengthPEDetected[OutParameters->nPEDetected] = PhotonWavelength[nPhotonSimulated-1];
          OutParameters->nPEDetected++;
          AddPulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
          if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
            AddAfterpulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
          }
          NpeVsTime[itime] = NpeVsTime[itime] + 1. + nIterationsPX;
          nIterationsPX = 0;
        }
      }
      if(((itime*Sampling == PulseTime)||( ( (itime-1)*Sampling < PulseTime)&&( itime*Sampling > PulseTime ) ))&&(SimACPulse)){
        //add AC pulse photons
        if(usePhotons){
          nIterationsPX = 0;
          AddPulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, nPhotonsPulse, Vbias - Vbd);
          if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
            AddAfterpulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, nPhotonsPulse, Vbias - Vbd);
          }
          nIterationsPX = 0;
          nIterationsPX = 0;
          nPEPulseWithDrop = nPhotonsPulse;
          nPEpulsesToSim = int(nPEPulseWithDrop);
          if( r1PDE->Uniform(1.) < nPEPulseWithDrop -  nPhotonsPulse) nPEpulsesToSim++;
          for(Int_t iSignalPhoton = 0; iSignalPhoton < nPEpulsesToSim; iSignalPhoton++){
            rPDE = r1PDE->Uniform(1.);
            if(f2->Eval(LightPulseWavelength, Overvoltage) >= rPDE) {
              nIterationsPX = 0;
              OutParameters->nPEDetectedACPulse++;
              AddPulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
              if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
                AddAfterpulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
              }
              nIterationsPX = 0;
            }
          }
        }
        else{
          nIterationsPX = 0;
          nPEPulseWithDrop = nPEPulse*(f2Drop->Eval(LightPulseWavelength, Overvoltage)/f2Drop->Eval(LightPulseWavelength, Vbias-Vbd));
          nPEpulsesToSim = int(nPEPulseWithDrop);
          if( r1PDE->Uniform(1.) < nPEPulseWithDrop -  nPEpulsesToSim) nPEpulsesToSim++;
          AddPulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, nPEpulsesToSim, Vbias - Vbd);
          if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
            AddAfterpulse(Waveform, WaveformAmpli, CurrentVsTimeGenerated, Wtime, nTimeIterations, mytemplate, Wtime[itime], 0.0, nPEpulsesToSim, Vbias - Vbd);
          }
          nIterationsPX = 0;
          nIterationsPX = 0;
          for(Int_t iSignalPhoton = 0; iSignalPhoton < nPEpulsesToSim; iSignalPhoton++){
            nIterationsPX = 0;
            OutParameters->nPEDetectedACPulse++;
            AddPulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
            if(r1Pap->Uniform(1.) < Pap(Overvoltage)){
              AddAfterpulse(WaveformDetected, WaveformDetectedAmpli, CurrentVsTimeDetected, Wtime, nTimeIterations, mytemplate, Wtime[itime], PxtVsTime[itime], Gain(Overvoltage)/GainInitial, Overvoltage);
            }
            nIterationsPX = 0;
          }
        }
      }

      PDEVsTime[itime] = f2->Eval(LightPulseWavelength, Overvoltage);

      // Compensation loop
      if(CompensationLoop){
      // use compensation loop
        if(itime>0){
            VbiasVsTime[itime] = VbiasVsTime[itime-1] + (VbiasVsTime[0] - Vbd - OvervoltageVsTime[itime]);
        }
        else{
            VbiasVsTime[itime] = VbiasVsTime[0];
        }
      }
      else{
        // compensation loop is not active
        VbiasVsTime[itime] = VbiasVsTime[0];
      }


      GainVsTime[itime] = Gain(Overvoltage);
      ADCVsTime[itime] = Ampli1peADC(Overvoltage);
    }

    Double_t IntegratedCharge = 0.0, AdditionalBaseline = 0.0, Rload = 50.;
    Int_t nPointsToCalc = nTimeIterations;
    if((PulseTime < nTimeIterations*Sampling)&&(SimACPulse)){
       nPointsToCalc = int(PulseTime/Sampling);
    }
    Double_t ActualLength = nPointsToCalc*Sampling;
    if(OverShootRatio>0.0){
      for(int itime=0; itime < nPointsToCalc; itime++){
        if(!Pap_On){
          IntegratedCharge += CurrentVsTimeDetected[itime]*Sampling;
        }
        else{
          IntegratedCharge += CurrentVsTimeDetected[itime]*Sampling*(1. + (Pap(OvervoltageVsTime[itime])/(1. - Pap(OvervoltageVsTime[itime]) )) );
        }

      }
      //cout<<"IntegratedCharge : "<<IntegratedCharge<<endl;
      IntegratedCharge = IntegratedCharge*OverShootRatio;
      AdditionalBaseline = Rload*IntegratedCharge/(Gfadc*ActualLength);
      //cout<<"Additional Baseline : "<<AdditionalBaseline<<endl;
      for(int itime=0; itime<nTimeIterations; itime++){
        WaveformDetectedAmpli[itime]+=AdditionalBaseline;
        WaveformDetectedAmpli[itime] = r1Elecl->Gaus(WaveformDetectedAmpli[itime], SigmaElectronic*Ampli1peADC( OvervoltageInitial ) );
        if (doInteger){
          WaveformDetectedAmpli[itime] = int(WaveformDetectedAmpli[itime]);
        }
      }
    }
    OutParameters->AdditionalBaseline = AdditionalBaseline;


    CurrentVsTime[0] = 0.0;
    CurrentVsTimeGenerated[0] = 0.0;
    CurrentVsTimeDetected[0] = 0.0;
    ADCVsTime[0] = Ampli1peADC(Overvoltage);


  OutParameters->Npoints = nTimeIterations;
  OutParameters->CurrentVsTimeDetectedAverage = 0.0;

    for(int itime=0; itime<nTimeIterations; itime++){

      OutParameters->Time[itime] = Wtime[itime];
      OutParameters->Current[itime] = CurrentVsTime[itime];
      OutParameters->VbiasVsTime[itime] = VbiasVsTime[itime];
      OutParameters->CurrentVsTimeGenerated[itime] = CurrentVsTimeGenerated[itime];
      OutParameters->CurrentVsTimeDetected[itime] = CurrentVsTimeDetected[itime];
      OutParameters->CurrentVsTimeDetectedAverage = OutParameters->CurrentVsTimeDetectedAverage + CurrentVsTimeDetected[itime];
      OutParameters->Npe[itime] = NpeVsTime[itime];
      OutParameters->WaveformPEGenerated[itime] = Waveform[itime];
      OutParameters->WaveformPEDetected[itime] = WaveformDetected[itime];
      OutParameters->WaveformAmpliDetected[itime] = WaveformDetectedAmpli[itime];
      OutParameters->WaveformAmpliDetectedInverted[itime] = -1.*WaveformDetectedAmpli[itime];
      OutParameters->Overvoltage[itime] = OvervoltageVsTime[itime];
      OutParameters->OvervoltageSimpleModel[itime] = OvervoltageSimpleVsTime[itime];
      OutParameters->PxtData[itime] = PxtVsTime[itime];
      OutParameters->PDESignalData[itime] = PDEVsTime[itime];
      OutParameters->GainData[itime] = GainVsTime[itime];
      OutParameters->AmplitudeData[itime] = ADCVsTime[itime];
      OutParameters->DCRData[itime] = DCRVsTime[itime];
    }
  OutParameters->CurrentVsTimeDetectedAverage = OutParameters->CurrentVsTimeDetectedAverage/nTimeIterations;

  OutParameters->Increment = TimeStep;
  OutParameters->SimfNSB = OutParameters->nPhotonSimAverage/(EndTime - StartTime);//fNSB;
  OutParameters->PDEtoNSBReal = AveragePDESimple/nAveragePDESimple;
  OutParameters->PDENSBEff = OutParameters->PDEtoNSBReal/OutParameters->AveragePDEforNSB;
  OutParameters->fNSBEfficiency = OutParameters->CalcNSB/OutParameters->SimfNSB;
  OutParameters->nPEEfficiency = OutParameters->nPhotonRecalculated/OutParameters->nPhotonSimAverage;
  //cout<<"OutParameters->nPhotonDetected : "<<OutParameters->nPhotonDetected<<endl;



  delete [] Waveform;
  delete [] WaveformDetected;
  delete [] WaveformAmpli;
  delete [] WaveformDetectedAmpli;
  delete [] Wtime;
  delete [] CurrentVsTime;
  delete [] VbiasVsTime;
  delete [] OvervoltageVsTime;
  delete [] PxtVsTime;
  delete [] PDEVsTime;
  delete [] GainVsTime;
  delete [] ADCVsTime;
  delete [] NpeVsTime;

  delete r0;
  delete r1PDE;
  delete r1;
  delete r1DCR;
  delete r1Elecl;
  delete[] xPhoton;
  delete[] PhotonWavelength;

}

void doFindPhotonWavelengthCorrected(MyNSB *NSB, Double_t *Range, Double_t *FoundWavelength){


  Int_t nRealUsedPoints = NSB->nPoint;

  if(nRealUsedPoints>1){
    Double_t *xW = new Double_t[nRealUsedPoints];
    Double_t *yI = new Double_t[nRealUsedPoints];
    nRealUsedPoints = 0;

    for(int point=0; point<NSB->nPoint; point++){
      if((NSB->Wavelength[point] >= Range[0] )&&( NSB->Wavelength[point] <= Range[1] )){
        xW[nRealUsedPoints] = NSB->Wavelength[point];
        if(nRealUsedPoints == 0) yI[nRealUsedPoints] = NSB->IntensityNormToWindow[point];
        else yI[nRealUsedPoints] = yI[nRealUsedPoints-1] + NSB->IntensityNormToWindow[point];
        nRealUsedPoints++;
      }
    }

    TRandom *r1Intensity = new TRandom();
    r1Intensity->SetSeed(0);
    Double_t rI = yI[nRealUsedPoints-1]*r1Intensity->Uniform(1.);

    while(yI[nRealUsedPoints-1]>rI){
      nRealUsedPoints--;
    }
    *FoundWavelength = xW[nRealUsedPoints];
    delete[] xW;
    delete[] yI;
    delete r1Intensity;
  }
  else{
    *FoundWavelength = NSB->Wavelength[0];
  }
  //cout<<*FoundWavelength<<endl;

}

void doFindPhotonWavelength(MyNSB *NSB, Double_t *Range, Double_t *FoundWavelength){

  Double_t NSBMax = -1.0;
  Int_t nRealUsedPoints = 0;
  for(int point=0; point<NSB->nPoint; point++){
    if((NSB->Wavelength[point] >= Range[0] )&&( NSB->Wavelength[point] <= Range[1] )){
      if(NSB->IntensityNormToWindow[point] > NSBMax) NSBMax = NSB->IntensityNormToWindow[point];
      nRealUsedPoints++;
    }
  }

  Double_t *xW = new Double_t[nRealUsedPoints];
  Double_t *yI = new Double_t[nRealUsedPoints];
  nRealUsedPoints=1;
  for(int point=0; point<NSB->nPoint-1; point++){
    //NSB->IntensityNormToWindow[point] = NSB->IntensityNormToWindow[point]/NSBMax;
    if((NSB->Wavelength[point] > Range[0] )&&( NSB->Wavelength[point] <= Range[1] )){
      xW[nRealUsedPoints] = 0.5*(NSB->Wavelength[point] + NSB->Wavelength[point-1]);
      if(nRealUsedPoints>0) yI[nRealUsedPoints] = yI[nRealUsedPoints-1] + NSB->IntensityNormToWindow[point];
      else yI[nRealUsedPoints] = NSB->IntensityNormToWindow[point];
      nRealUsedPoints++;
    }
  }

  TRandom *r1Intensity = new TRandom();
  r1Intensity->SetSeed(0);
  Double_t rI = yI[nRealUsedPoints-1]*r1Intensity->Uniform(1.);

/*  TRandom *r1Wavelength = new TRandom();
  r1Wavelength->SetSeed(0);
  Double_t rW = Range[0] + r1Wavelength->Uniform(Range[1] - Range[0]);
*/
  nRealUsedPoints=0;
  while ( rI>=yI[nRealUsedPoints] ) {
    nRealUsedPoints++;
  }
  //cout<<"Foud Wavelength : "<<xW[nRealUsedPoints]<<endl;
  *FoundWavelength = xW[nRealUsedPoints];
  //*FoundWavelength = 470.;

}

void AddAfterpulse(Double_t *Y, Double_t *YDetected, Double_t *Current, Double_t *X, Int_t nP, MyTemplate *mytemplate, Double_t mytiem, Double_t Pxt, Double_t peAmplitude, Double_t Overvoltage){

  TRandom3 *r1Pap = new TRandom3();
  r1Pap->SetSeed(0);

  Double_t Pap = r1Pap->Uniform(-1./(PapTau));
  Double_t Amplitude = 1 - TMath::Exp(-Pap/(5.*TemplateTau));
  //cout<<"Time : "<<Pap <<" Amplitude : "<< Amplitude <<endl;

  AddPulse(Y, YDetected, Current, X, nP, mytemplate, mytiem + Pap, Pxt, Amplitude*peAmplitude, Overvoltage);


}

void AddPulse(Double_t *Y, Double_t *YDetected, Double_t *Current, Double_t *X, Int_t nP, MyTemplate *mytemplate, Double_t mytiem, Double_t Pxt, Double_t peAmplitude, Double_t Overvoltage){

  Int_t tp = mytiem/Sampling;
  //Double_t ChargeAmpli = peAmplitude*Cucell*(Vbias-Vbd)/mytemplate->ChargeToNorm;
  Double_t ChargeAmpli = peAmplitude*Cucell*(Vbias-Vbd)/mytemplate->ChargeToNormSpline;

  TRandom3 *r1PE = new TRandom3();
  r1PE->SetSeed(0);

  TRandom3 *r1TimeShift = new TRandom3();
  r1TimeShift->SetSeed(0);
  Double_t TimeShift = r1TimeShift->Uniform(Sampling);
  Int_t nPointsShift = int(TimeShift/TemplateSampling);
  //cout<<"TimeShift : "<<TimeShift<<" nP shift "<<nPointsShift<<endl;

  peAmplitude = r1PE->Gaus(peAmplitude, peAmplitude*SigmaGain);
  Double_t peAmpliDetected = r1PE->Gaus( Ampli1peADC(Overvoltage), SigmaGain*Ampli1peADC(OvervoltageInitial));
  //cout<<" mytemplate->nTemplateUsfullPoints-1 : "<<mytemplate->nTemplateUsfullPoints-1<<endl;

  for(Int_t point=0; point<mytemplate->nTemplateUsfullPoints-1; point++){
    if(tp+point<nP) {
      Y[tp+point] = Y[tp+point] + peAmplitude*(mytemplate->AmpliSpline[int((mytemplate->OneTemplateStep)*point + nPointsShift)] );
      YDetected[tp+point] = YDetected[tp+point] + peAmpliDetected*(mytemplate->AmpliSpline[int((mytemplate->OneTemplateStep)*point + nPointsShift)]);
      //Current[tp+point] = Current[tp+point] + (ChargeAmpli)*(mytemplate->AmpliSpline[int((mytemplate->OneTemplateStep)*point + nPointsShift)])/NormFactor;
      Current[tp+point] = Current[tp+point] + (ChargeAmpli)*(mytemplate->AmpliSpline[int((mytemplate->OneTemplateStep)*point)])/NormFactor;
    }
  }


//  cout<<"do XT : "<<Pxt<<" "<<rPxt;
  if(Pxt > 0.0){
    nPulses++;
    if(nIterationsPX < nMaxPxt){
      TRandom1 *r1 = new TRandom1();
      r1->SetSeed(0);
      Double_t rPxt = r1->Uniform(1.);
      //cout<<"do XT : "<<Pxt<<" "<<rPxt;
      if(Pxt > rPxt){
        nXTPulses++;
        delete r1;
        nIterationsPX++;
        AddPulse(Y, YDetected, Current, X, nP, mytemplate, mytiem, Pxt, peAmplitude, Overvoltage);
      }
      else delete r1;
    }
    else{
      cout<<"Error : Reach Max available Crosstalk photons = "<<nMaxPxt<<endl;
    }

  }
  delete r1PE;
  //delete r1TimeShift;
  //cout<<endl;

}

void SetTransparentPadBottom(TVirtualPad *overlay){

  overlay->SetFillStyle(4000);
  overlay->SetFillColor(0);
  overlay->SetFrameFillStyle(4000);
  overlay->Draw();
  overlay->cd();
  overlay->SetLeftMargin(0.18);
  overlay->SetRightMargin(0.18);
  //overlay->SetRightMargin(0.15);
  //overlay->SetLeftMargin(0.18);
  //overlay->SetBottomMargin(0.35);
  //overlay->SetTopMargin(0.00001);
  overlay->SetBorderMode(0);

}

void SetAxisToDraw(TGaxis *axis, TString title){

  axis->SetLineColor(2);
  axis->SetLabelColor(2);
  axis->SetLabelSize(0.04);
  axis->SetTitle(title);
  axis->CenterTitle();
  axis->SetTitleSize(0.04);
  axis->SetTitleOffset(1.1);
  axis->SetTitleColor(2);
}

void SetTopFrame(TVirtualPad *pad1, TString Xtytle, TString Ytytle, bool logyscale, Double_t *XRange, Double_t *YRange){

  if(logyscale) pad1->SetLogy();
  pad1->Draw();
  pad1->cd();
  pad1->SetLeftMargin(0.18);
  pad1->SetRightMargin(0.18);
  TH1F *Top = pad1->DrawFrame(XRange[0], YRange[0], XRange[1], YRange[1]);

  Top->SetXTitle(Xtytle);
  Top->SetYTitle(Ytytle);
  Top->GetYaxis()->CenterTitle();

}

void FindAmpliGain(MyTemplate *mytemplate) {
  Double_t Vin = 0.5, Vmax = 7.5, Vstep = 0.05;
  Int_t nSteps = (Vmax - Vin)/Vstep;
  Double_t Overvoltage;

  TH1D *h1AmpliGain = new TH1D("h1AmpliGain", "Ampli Gain", nSteps, 80, 90);
  for(Int_t ipoint = 0; ipoint < nSteps; ipoint++){
    Overvoltage = Vin + Vstep*ipoint;
    //cout<<(mytemplate->ChargeToNorm*Ampli1pe*Gain(Overvoltage)/Gain(Vbias-Vbd) )/(Cucell*Overvoltage)<<endl;
    h1AmpliGain->Fill( (mytemplate->ChargeToNorm*Ampli1peADC(Overvoltage) )/(Cucell*Overvoltage) );
  }
  TCanvas *c1 = new TCanvas("c1", "", 800, 800);
  h1AmpliGain->Draw();
  c1->SaveAs("AmliGain.root");

}

void SetTemplateTxt(TString filename, MyTemplate *mytemplate, TString path2){

  ifstream inputpar;
  inputpar.open(filename);
  assert(inputpar.is_open());
  TString parname;
  Int_t n = 0;
  while ( !inputpar.eof() ){
    inputpar>>mytemplate->Time[n];
    mytemplate->Time[n] = mytemplate->Time[n]*1.e-9;
    inputpar>>mytemplate->Amplitude[n];
    n++;
  }
  mytemplate->NofPoints = n;

  Double_t charge = 0.0;
  for(int i=0; i<mytemplate->NofPoints; i++){
     mytemplate->Amplitude[i] = mytemplate->Amplitude[i];
     charge = charge + mytemplate->Amplitude[i]*(mytemplate->Time[1]- mytemplate->Time[0]);
     //charge = charge + mytemplate->Amplitude[i]*1.e-9;
  }
  mytemplate->ChargeToNorm = charge;
  cout<<"Template Q : "<<charge<<endl;

  /*MySmooth(mytemplate->Amplitude, mytemplate->Current, mytemplate->NofPoints, 15);

  Double_t chargeSmooth = 0.0;
  for(int i=0; i<mytemplate->NofPoints; i++){
     chargeSmooth = chargeSmooth + mytemplate->Current[i]*1.e-9;
     //charge = charge + mytemplate->Amplitude[i]*1.e-9;
  }
  cout<<"chargeSmooth : "<<chargeSmooth<<endl;*/


  TCanvas *ctest = new TCanvas("ctest", "", 800, 800);
  TGraph *grTemplate = new TGraph(mytemplate->NofPoints-1, mytemplate->Time, mytemplate->Amplitude);
  grTemplate->SetLineColor(4);
  grTemplate->SetMarkerColor(4);
  grTemplate->Draw("APL");


  Int_t npTemplate = mytemplate->NofPoints-1;
  std::vector<double> Xtime(npTemplate), YTemplateSpline(npTemplate);
  for(int i=0; i<npTemplate; i++){
    Xtime[i] = mytemplate->Time[i];
    YTemplateSpline[i] = mytemplate->Amplitude[i];
  }
  tk::spline sAmplitueSim;
  sAmplitueSim.set_points(Xtime, YTemplateSpline);

  Double_t tStrat = 0.0, tEnd = mytemplate->Time[npTemplate-1], tStep = TemplateSampling;
  Int_t iPoint = (tEnd-tStrat)/tStep;
  if(iPoint>10000){
    cout<<"Error : n Template Spline points : "<<iPoint<< ">> 10K"<<endl;
    iPoint = 10000;
    cout<<"Error : n Template Spline points set to :"<<iPoint<<endl;
  }

  Double_t *xInt = new Double_t[iPoint];
  Double_t *ySpline = new Double_t[iPoint];

  mytemplate->nPointsSpline = iPoint;
  cout<<mytemplate->nPointsSpline<<endl;

  for(int i = 0; i<iPoint; i++){
    xInt[i] = tStrat + tStep*i;
    ySpline[i] = sAmplitueSim(xInt[i]);
    mytemplate->TimeSpline[i] = xInt[i];
    mytemplate->AmpliSpline[i] = ySpline[i];
  }

  mytemplate->nTemplateUsfullPoints = (mytemplate->nPointsSpline)*(mytemplate->TimeSpline[1] - mytemplate->TimeSpline[0])/Sampling;
  mytemplate->OneTemplateStep = int(Sampling/TemplateSampling);
  cout<<"mytemplate->OneTemplateStep : "<<mytemplate->OneTemplateStep<<endl;

  Double_t chargeSpline = 0.0;

  for(Int_t point=0; point<mytemplate->nTemplateUsfullPoints-1; point++){
      chargeSpline += mytemplate->AmpliSpline[int((mytemplate->OneTemplateStep)*point)]*Sampling;
  }

  mytemplate->ChargeToNormSpline = chargeSpline;
  cout<<"Template Q Spline : "<<chargeSpline<<endl;

  TGraph *grTemplateSpline = new TGraph(iPoint, mytemplate->TimeSpline, mytemplate->AmpliSpline);
  grTemplateSpline->SetLineColor(2);
  grTemplateSpline->SetMarkerColor(2);
  grTemplateSpline->Draw("P");

  ctest->SaveAs("templateTest.root");

}

void SetTemplate(TString path, MyTemplate *mytemplate){

    Int_t NTTemplateP=10000, tmpTemperature;
    Double_t tau=0.0, Charge1tau, Charge3tau, Charge5tau, risetime, GaindB;

    Double_t *YTemplate = new Double_t[NTTemplateP];

    TFile *ftempl = new TFile(path);
    TTree *t3 = (TTree*)ftempl->Get("T");


    t3->SetBranchAddress("ShortLength",&NTTemplateP);
    t3->SetBranchStatus("ShortLength", 1);
    t3->SetBranchAddress("VTemplate",YTemplate);
    t3->SetBranchStatus("VTemplate", 1);
    t3->SetBranchAddress("tau_ns",&tau);
    t3->SetBranchStatus("tau_ns", 1);
    t3->SetBranchAddress("RiseTime",&risetime);
    t3->SetBranchStatus("RiseTime", 1);
    t3->SetBranchAddress("Charge5tau",&Charge5tau);
    t3->SetBranchStatus("Charge5tau", 1);
    t3->SetBranchAddress("Charge3tau",&Charge3tau);
    t3->SetBranchStatus("Charge3tau", 1);
    t3->SetBranchAddress("Charge1tau",&Charge1tau);
    t3->SetBranchStatus("Charge1tau", 1);
    t3->SetBranchAddress("Temperature",&tmpTemperature);
    t3->SetBranchStatus("Temperature", 1);
    t3->SetBranchAddress("AmolifierGaindB",&GaindB);
    t3->SetBranchStatus("AmolifierGaindB", 1);
    t3->GetEntry(0);

    mytemplate->NofPoints = NTTemplateP;
    mytemplate->Temperature = tmpTemperature;
    mytemplate->Tau = tau*1.e-9;
    //mytemplate->RiseTime = risetime;
    mytemplate->Charge5tau = Charge5tau;
//    mytemplate->Charge4tau = Charge4tau;
    mytemplate->Charge3tau = Charge3tau;
//    mytemplate->Charge2tau = Charge2tau;
    mytemplate->Charge1tau = Charge1tau;
    cout<<mytemplate->NofPoints<<endl;

    Double_t charge = 0.0;
    Double_t Max = 0.0;
    Int_t StartNpoint = 73;

    for(int i=StartNpoint; i<(mytemplate->NofPoints); i++){
      if(-YTemplate[i] >= 0.0){
          mytemplate->Amplitude[i-StartNpoint] = -YTemplate[i];
      }
      else{
        mytemplate->Amplitude[i-StartNpoint] = 0.0;
      }
       mytemplate->Time[i-StartNpoint] = i*0.4e-9 - 141.2e-9 + 0.112e-6;// - mytemplate->RiseTime;
       if(mytemplate->Time[i-StartNpoint] > 0.2e-6) mytemplate->Amplitude[i] = 0.0;
       charge = charge + mytemplate->Amplitude[i-StartNpoint]*0.4e-9;
       if(mytemplate->Amplitude[i-StartNpoint] > Max) Max = mytemplate->Amplitude[i-StartNpoint];
       //cout<<YTemplate[i]<<endl;
    }

    cout<<"Charge : "<<charge<<endl;
    cout<<"Max : "<<Max<<endl;
    mytemplate->NofPoints = mytemplate->NofPoints - StartNpoint;

    charge = 0.0;
    for(int i=0; i<mytemplate->NofPoints; i++){
       mytemplate->Amplitude[i] = mytemplate->Amplitude[i]/Max;
       charge = charge + mytemplate->Amplitude[i]*0.4e-9;
    }
    mytemplate->ChargeToNorm = charge;///pow(10, GaindB/20);
    mytemplate->Gain = pow(10, GaindB/20);
    cout<<"Charge : "<<mytemplate->ChargeToNorm<<endl;
    //cout<<"Ampli1pe : "<<Ampli1pe<<endl;
    cout<<"Ampli1peADC : "<<Ampli1peADC(Vbias - Vbd)<<endl;
    //cout<<"1 p.e. charge Template : "<<mytemplate->ChargeToNorm*Ampli1pe<<endl;
    cout<<"1 p.e. charge Cucell   : "<<Cucell*(Vbias-Vbd)<<endl;
    //cout<<"Ampli Gain : "<<(mytemplate->ChargeToNorm*Ampli1pe)/(Cucell*(Vbias-Vbd))<<endl;
    /*for(int i=0; i<mytemplate->NofPoints; i++){
       //mytemplate->Amplitude[i] = mytemplate->Amplitude[i]/charge;
       //cout<<YTemplate[i]<<endl;
    }*/

    ftempl->Close();

    delete [] YTemplate;
    delete ftempl;
}

void DoCalcCorrectedCharge(MyTemplate *mytemplate, Double_t *Correction){

  Double_t charge = 0.0;
  for(Int_t point=1; point<mytemplate->NofPoints; point++){
    for(Int_t i = point; i<mytemplate->NofPoints; i++){
       charge = charge + mytemplate->Amplitude[i]*Sampling;
    }
  }
  *Correction = charge;

}

void Solve2Equat(Double_t a, Double_t b, Double_t c, Double_t *Result){
  Double_t Descriminant = b*b - 4.*a*c;
  if(Descriminant>=0.0){
      Result[0] = (-b - TMath::Sqrt( Descriminant ))/(2.*a);
      Result[1] = (-b + TMath::Sqrt( Descriminant ))/(2.*a);
  }
  else {
    cout<<"Wrong equation"<<endl;
  }
  cout<<b<<" "<<Descriminant<<" "<<TMath::Sqrt( Descriminant )<<endl;

}

void MySmooth(Double_t *array, Double_t *SmoothArray,
 Int_t NomberOfEllements, Int_t HalfOfSumm){

    Int_t i;
    Double_t sum = 0.0;
    for( i = 0; i < NomberOfEllements; i++){
        sum=sum+array[i];
        if(i>2*HalfOfSumm) sum=sum - array[i-2*HalfOfSumm];
        SmoothArray[i] = sum/(2*HalfOfSumm);
    }
}
