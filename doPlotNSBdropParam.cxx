#include <TH1D.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TString.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include "TChain.h"
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TCut.h>
#include <TF1.h>
#include <TF2.h>
#include <TLegend.h>
#include <TMath.h>
#include <TLatex.h>
#include "Fit/Fitter.h"
#include "Fit/BinData.h"
#include "HFitInterface.h"
#include "TVirtualFitter.h"

//C, C++
#include <stdio.h>
//#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>
#include <sstream>

using namespace std;

Double_t PDEMax(Double_t *x, Double_t *par){

  Double_t fitval;
  if((x[0]<=370)&&(x[0]>=260)){
     fitval = par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
  }
  else if( (x[0]<=530)&&(x[0]>=370) ){
     fitval = par[3] + par[4]*x[0] + par[5]*x[0]*x[0];
  }
  else if( (x[0]<800)&&(x[0]>=530) ){
     fitval = par[6] + par[7]*x[0] + par[8]*x[0]*x[0];
  }
  else if( (x[0]<1000)&&(x[0]>=800) ){
     fitval = par[9] + par[10]*x[0] + par[11]*x[0]*x[0];
  }
  return fitval;
}

Double_t PDESlope(Double_t *x, Double_t *par){
   Double_t fitval;
   fitval = par[0] + par[1]*x[0] + par[2]*x[0]*x[0]; //+ par[3]*x[0]*x[0]*x[0];
   return fitval;
}

Double_t fun2(Double_t *x, Double_t *par) {

   // x[0] = wavelegth;
   // x[1] = Overvoltage;
   //PDEFit = PDEMax - the PDE at saturation;
   //PDESlope =

   Double_t *p1 = &par[0];
   Double_t *p2 = &par[12];
   Double_t result = PDEMax(&x[0], p1)*(1 - TMath::Exp( -PDESlope(&x[0], p2)*x[1] ) );
   return result;
}

Double_t fun2Drop(Double_t *x, Double_t *par) {

   // x[0] = wavelegth;
   // x[1] = Overvoltage;
   //PDEFit = PDEMax - the PDE at saturation;
   //PDESlope =

   Double_t *p1 = &par[0];
   Double_t *p2 = &par[12];
   Double_t result = PDEMax(&x[0], p1)*(1 - TMath::Exp( -PDESlope(&x[0], p2)*x[1] ) );
   return result;
}

void doPlotNSBdropParam(){

  Double_t BaselineShift = 10.0;
  Double_t InitialOvervoltage = 2.8;

  Double_t NSBvsBaselinePar[4];
    NSBvsBaselinePar[0] = 0.0;
    NSBvsBaselinePar[1] = 4.54799e+06;
    NSBvsBaselinePar[2] = 69246.;
    NSBvsBaselinePar[3] = -133.048;
  TF1 *f1NSBvsBaselineShift = new TF1("f1NSBvsBaselineShift", "pol3", 0.0, 80.);
  for(Int_t i=0; i<4; i++){
      f1NSBvsBaselineShift->SetParameter(i, NSBvsBaselinePar[i]);
  }
  cout<<"NSB : "<<f1NSBvsBaselineShift->Eval(BaselineShift)<<endl;

  Double_t VdropVsBaselinePar[3];
    VdropVsBaselinePar[0] = 0.0;
    VdropVsBaselinePar[1] = 0.0157851;
    VdropVsBaselinePar[2] = -1.66469e-05;
  TF1 *f1VdropvsBaselineShift = new TF1("f1VdropvsBaselineShift", "pol2", 0.0, 80.);
  for(Int_t i=0; i<3; i++){
      f1VdropvsBaselineShift->SetParameter(i, VdropVsBaselinePar[i]);
  }
  cout<<"Vdrop : "<<f1VdropvsBaselineShift->Eval(BaselineShift)<<endl;

  Double_t Ampli1peDropPar[3];
    Ampli1peDropPar[0] = 5.01924;
    Ampli1peDropPar[1] = -0.0287831;
    Ampli1peDropPar[2] = 3.59506e-05;
  TF1 *f1Ampli1PEvsBaselineShift = new TF1("f1Ampli1PEvsBaselineShift", "pol2", 0.0, 80.);
  for(Int_t i=0; i<3; i++){
      f1Ampli1PEvsBaselineShift->SetParameter(i, Ampli1peDropPar[i]);
  }
  cout<<"1 p.e. Amplitude : "<<f1Ampli1PEvsBaselineShift->Eval(BaselineShift)<<endl;

  Double_t Gain1peDropPar[3];
    Gain1peDropPar[0] = 1.48777e+06;
    Gain1peDropPar[1] = -8488.54;
    Gain1peDropPar[2] = 10.1111;
  TF1 *f1Gain1PEvsBaselineShift = new TF1("f1Gain1PEvsBaselineShift", "pol2", 0.0, 80.);
  for(Int_t i=0; i<3; i++){
      f1Gain1PEvsBaselineShift->SetParameter(i, Gain1peDropPar[i]);
  }
  cout<<"1 p.e. Gain : "<<f1Gain1PEvsBaselineShift->Eval(BaselineShift)<<endl;

  Double_t PxtDropPar[3];
    PxtDropPar[0] = 0.065667;
    PxtDropPar[1] = -0.000666647;
    PxtDropPar[2] = 1.96339e-06;
  TF1 *f1PxtvsBaselineShift = new TF1("f1PxtvsBaselineShift", "pol2", 0.0, 80.);
  for(Int_t i=0; i<3; i++){
      f1PxtvsBaselineShift->SetParameter(i, PxtDropPar[i]);
  }
  cout<<"Pxt : "<<f1PxtvsBaselineShift->Eval(BaselineShift)<<endl;


  Double_t PxtRelativeDropPar[4];
    PxtRelativeDropPar[0] = 1.;
    PxtRelativeDropPar[1] = -0.00939331;
    PxtRelativeDropPar[2] = 3.38815e-06;
    PxtRelativeDropPar[3] = 2.37228e-07;
  TF1 *f1PxtRelativevsBaselineShift = new TF1("f1PxtRelativevsBaselineShift", "pol3", 0.0, 80.);
  for(Int_t i=0; i<4; i++){
      f1PxtRelativevsBaselineShift->SetParameter(i, PxtRelativeDropPar[i]);
  }
  cout<<"Pxt : "<<f1PxtRelativevsBaselineShift->Eval(BaselineShift)<<endl;

  Double_t SigmaToNorm = 0.05813;


  TF1 *fSigmaNorm = new TF1("fSigma", "pol3", 1., 4.);
  fSigmaNorm->SetParameter(0, 0.373786/SigmaToNorm);
  fSigmaNorm->SetParameter(1, -0.299333/SigmaToNorm);
  fSigmaNorm->SetParameter(2, 0.0939171/SigmaToNorm);
  fSigmaNorm->SetParameter(3, -0.00974216/SigmaToNorm);

  cout<<"Sigma Gain Relative : "<<fSigmaNorm->Eval(InitialOvervoltage - f1VdropvsBaselineShift->Eval(BaselineShift))<<endl;

  const Int_t npar = 15;
  Double_t f2params[npar] =
     {-1.29693e+00, 8.27659e-03, -1.02035e-05, -1.89539e+00, 9.78309e-03, -9.97473e-06, 1.25334e+00, -1.53717e-03, 1.96296e-07,
      2.09973e+00, -3.90231e-03, 1.82918e-06, 4.96106e-01, -4.65596e-05, -3.03291e-07};
   TF2 *f2 = new TF2("f2",fun2, 260, 800, 0.2, 7., npar);
   f2->SetParameters(f2params);

   TCanvas *c1PDECanvas = new TCanvas("c1PDECanvas", "", 800., 800.);
   c1PDECanvas->Divide(2,2);
   c1PDECanvas->cd(1)->SetGrid();
   f2->Draw("surf1");

   Double_t StartWavelength = 260.;
   Double_t EndWavelength = 810.;
   Double_t Step = 10.;
   Int_t nPoints = (EndWavelength - StartWavelength)/Step;

   Double_t *Wavelength = new Double_t[nPoints];
   Double_t *PDE = new Double_t[nPoints];

   auto str = std::to_string(BaselineShift);
   cout<<"str : "<<str<<endl;
   TString OutFileName = "Baseline_" + str + ".txt";

   ofstream fout(OutFileName, ios::app);

   for(Int_t iWL = 0; iWL < nPoints; iWL++){
     Wavelength[iWL] = StartWavelength + iWL*Step;
     PDE[iWL] = f2->Eval(Wavelength[iWL], InitialOvervoltage - f1VdropvsBaselineShift->Eval(BaselineShift));
     //fout<<Wavelength[iWL]<<"\t"<<PDE[iWL]<<endl;
   }

   Double_t StartWL = 260., EndWL = 800., StepWL = 10.;
   Int_t nWLPoints = (EndWL - StartWL)/StepWL;


   Double_t StartVdrop = 0.0, EndWdrop = 1.4, StepVdrop = 0.01;
   Int_t nOVPoints = (EndWdrop - StartVdrop)/StepVdrop;
   Double_t MydV, MyWL, MyBL;

   Double_t VdropToBaselinePar[3];
     VdropToBaselinePar[0] = 0.;
     VdropToBaselinePar[1] = 62.7521;
     VdropToBaselinePar[2] = 5.62274;

   TF1 *f1BaseLineVsVdrop = new TF1("f1BaseLineVsVdrop", "pol2", 0.0, 1.4);
   for(Int_t i=0; i<3; i++){
       f1BaseLineVsVdrop->SetParameter(i, VdropToBaselinePar[i]);
   }

   Double_t VdropToBaselinePar24k[2];
     VdropToBaselinePar24k[0] = 0.;
     VdropToBaselinePar24k[1] = 306.474;
   TF1 *f1BaseLineVsVdrop24k = new TF1("f1BaseLineVsVdrop24k", "pol1", 0.0, 0.4);

   for(Int_t i=0; i<2; i++){
      f1BaseLineVsVdrop24k->SetParameter(i, VdropToBaselinePar24k[i]);
   }

   Double_t StartBL = f1BaseLineVsVdrop->Eval(StartVdrop), EndBL = f1BaseLineVsVdrop->Eval(EndWdrop), StepBL = f1BaseLineVsVdrop->Eval(StepVdrop);
   cout<<"Start BL : "<<StartBL<<" End BL : "<<EndBL<<endl;

   TH2D *h2dPDEDrop = new TH2D("h2dPDEDrop", "PDE drop", nWLPoints, StartWL, EndWL, nOVPoints, StartVdrop, EndWdrop);
   TH2D *h2dPDEDropBL = new TH2D("h2dPDEDropBL", "PDE drop", nWLPoints, StartWL, EndWL, nOVPoints, StartBL, EndBL);
   h2dPDEDropBL->GetXaxis()->SetTitle("Wavelength, nm");
   h2dPDEDropBL->GetYaxis()->SetTitle("Baselie Shift");
   h2dPDEDropBL->GetZaxis()->SetTitle("PDE");

   Double_t StartNSB = f1NSBvsBaselineShift->Eval(StartBL);
   Double_t EndNSB = f1NSBvsBaselineShift->Eval(EndBL);
   TH2D *h2dPDEDropNSB = new TH2D("h2dPDEDropNSB", "PDE drop", nWLPoints, StartWL, EndWL, nOVPoints, StartNSB, EndNSB);
   h2dPDEDropNSB->GetXaxis()->SetTitle("Wavelength, nm");
   h2dPDEDropNSB->GetYaxis()->SetTitle("NSB, p.e./s");
   h2dPDEDropNSB->GetZaxis()->SetTitle("PDE");

   TH2D *h2dPDEDropNSB24k = new TH2D("h2dPDEDropNSB24k", "PDE drop, 2.4k", nWLPoints, StartWL, EndWL, nOVPoints, StartNSB, EndNSB);
   h2dPDEDropNSB24k->GetXaxis()->SetTitle("Wavelength, nm");
   h2dPDEDropNSB24k->GetYaxis()->SetTitle("NSB, p.e./s");
   h2dPDEDropNSB24k->GetZaxis()->SetTitle("PDE");

   TH2D *h2dPDEDropBL24k = new TH2D("h2dPDEDropBL24k", "PDE drop", nWLPoints, StartWL, EndWL, nOVPoints, StartBL, EndBL);
   h2dPDEDropBL24k->GetXaxis()->SetTitle("Wavelength, nm");
   h2dPDEDropBL24k->GetYaxis()->SetTitle("Baselie Shift");
   h2dPDEDropBL24k->GetZaxis()->SetTitle("PDE");

   for(Int_t iWL = 0; iWL < nWLPoints; iWL++){
     MyWL = StartWL + iWL*StepWL;
     for(Int_t iOverV = 0; iOverV < nOVPoints; iOverV++){
       MydV = StartVdrop + StepVdrop*iOverV;
       MyBL = f1BaseLineVsVdrop->Eval(MydV);
       h2dPDEDrop->SetBinContent(iWL, iOverV, f2->Eval(MyWL, InitialOvervoltage - MydV) );
       h2dPDEDropBL->SetBinContent(iWL, iOverV, f2->Eval(MyWL, InitialOvervoltage - MydV) );
       h2dPDEDropNSB->SetBinContent(iWL, iOverV, f2->Eval(MyWL, InitialOvervoltage - MydV) );
     }
   }


   StartVdrop = 0.0, EndWdrop = 0.4, StepVdrop = 0.003;
   nOVPoints = (EndWdrop - StartVdrop)/StepVdrop;

   for(Int_t iWL = 0; iWL < nWLPoints; iWL++){
     MyWL = StartWL + iWL*StepWL;
     for(Int_t iOverV = 0; iOverV < nOVPoints; iOverV++){
       MydV = StartVdrop + StepVdrop*iOverV;
       MyBL = f1BaseLineVsVdrop24k->Eval(MydV);
       h2dPDEDropBL24k->SetBinContent(iWL, iOverV, f2->Eval(MyWL, InitialOvervoltage - MydV) );
       h2dPDEDropNSB24k->SetBinContent(iWL, iOverV, f2->Eval(MyWL, InitialOvervoltage - MydV) );
     }
   }

   c1PDECanvas->cd(3)->SetGrid();
   h2dPDEDrop->Draw("surf1");

   c1PDECanvas->cd(4)->SetGrid();
   h2dPDEDropBL->Draw("surf1");

   TCanvas *cPDEDropVsBaseline = new TCanvas("cPDEDropVsBaseline", "PDE drop vs. lambda and Baseline", 800., 800.);
   h2dPDEDropBL->Draw("surf1");
   cPDEDropVsBaseline->SaveAs("PDEDropVsBaseline.root");

   TCanvas *cPDEDropVsBaseline24k = new TCanvas("cPDEDropVsBaseline24k", "PDE drop vs. lambda and Baseline", 800., 800.);
   h2dPDEDropBL24k->Draw("surf1");
   cPDEDropVsBaseline24k->SaveAs("PDEDropVsBaseline24k.root");


   TCanvas *cPDEDropVsNSB = new TCanvas("cPDEDropVsNSB", "PDE drop vs. lambda and Baseline", 800., 800.);
   cPDEDropVsNSB->Divide(2, 1);
   cPDEDropVsNSB->cd(1)->SetGrid();
   h2dPDEDropNSB->Draw("Lego2");
   cPDEDropVsNSB->cd(2)->SetGrid();
   h2dPDEDropNSB24k->Draw("Lego2");

   cPDEDropVsNSB->SaveAs("PDEDropVsNSB.root");


   TCanvas *cPDEDropVsNSB24k = new TCanvas("cPDEDropVsNSB24k", "PDE drop vs. lambda and Baseline, 2.4k", 800., 800.);
   h2dPDEDropNSB24k->Draw("surf1");
   cPDEDropVsNSB24k->SaveAs("PDEDropVsNSB24k.root");



   c1PDECanvas->cd(2)->SetGrid();

   TGraph *grPDEdrop = new TGraph(nPoints, Wavelength, PDE);
   grPDEdrop->Draw("APL");

   TCanvas *cSNR = new TCanvas("cSNR", "cSNR", 800., 800.);
   cSNR->Divide(1,2);
   cSNR->cd(1)->SetGrid();

   Double_t BaselineArr[100], NSBGammas[100];
   Double_t SNR[100];
   Double_t sigma;
   Double_t sigmaE = 0.2024*f1Ampli1PEvsBaselineShift->Eval(0.0);
   Double_t sigmaS = 0.09*f1Ampli1PEvsBaselineShift->Eval(0.0);

   Double_t GammasVsBaselinePar[4];
   GammasVsBaselinePar[0] = 0.;
   GammasVsBaselinePar[1] = 1.8618e+07;
   GammasVsBaselinePar[2] = 60009.5;
   GammasVsBaselinePar[3] = 3000.03;
   TF1 *f1GammasVsBaselineShift = new TF1("f1GammasVsBaselineShift", "pol3", 0.0, 80.);

   for(Int_t i=0; i<4; i++){
     f1GammasVsBaselineShift->SetParameter(i, GammasVsBaselinePar[i]);
   }

   for(Int_t ip = 0; ip < 70; ip++){
     BaselineArr[ip] = ip;
     sigma = TMath::Sqrt( sigmaE*sigmaE + (sigmaS*(fSigmaNorm->Eval(InitialOvervoltage - f1VdropvsBaselineShift->Eval(BaselineArr[ip]))) )*(sigmaS*(fSigmaNorm->Eval(InitialOvervoltage - f1VdropvsBaselineShift->Eval(BaselineArr[ip]))) ) );
     SNR[ip] = (f1Ampli1PEvsBaselineShift->Eval(BaselineArr[ip]))/(sigma) ;
     NSBGammas[ip] = f1GammasVsBaselineShift->Eval(BaselineArr[ip]);
   }

   TGraph *grSNR = new TGraph(70, BaselineArr, SNR);
   grSNR->SetTitle("");
   grSNR->GetXaxis()->SetTitle("Baseline Shift, ADC");
   grSNR->GetYaxis()->SetTitle("A_{1p.e.}/(#sqrt{ #sigma_{el.}^{2}(Const.) + #sigma_{gain}^{2}(BL_{shift}) } )");
   grSNR->Draw("APL");

   cSNR->cd(2)->SetGrid();
   TGraph *grSNRvsNSB = new TGraph(70, NSBGammas, SNR);
   grSNRvsNSB->SetTitle("");
   grSNRvsNSB->GetXaxis()->SetTitle("NSB, #gamma/s");
   grSNRvsNSB->GetYaxis()->SetTitle("A_{1p.e.}/(#sqrt{ #sigma_{el.}^{2}(Const.) + #sigma_{gain}^{2}(NSB) } )");
   grSNRvsNSB->Draw("APL");

   Double_t MyNSBpeStart = 0.0, MyNSBpeEnd = 1.e9, MyNSBpeSTEP = 10.e6;
   Int_t nSBDpoints = (MyNSBpeEnd - MyNSBpeStart)/MyNSBpeSTEP;

   TF1 *fPxtVsPE = new TF1("fPxtVsPE", "pol3", 0.0, 0.5e9);
   fPxtVsPE->SetParameter(0, 1.);
   fPxtVsPE->SetParameter(1, -1.152e-10/0.0651074);
   fPxtVsPE->SetParameter(2, 1.43803e-19/0.0651074);
   fPxtVsPE->SetParameter(2, -8.92112e-29/0.0651074);

   TF1 *fPxtVsPE24k = new TF1("fPxtVsPE24k", "pol3", 0.0, 0.5e9);
   fPxtVsPE24k->SetParameter(0, 1.);
   fPxtVsPE24k->SetParameter(1, -2.8037e-11/0.0654841);
   fPxtVsPE24k->SetParameter(2, 9.53674e-21/0.0654841);
   fPxtVsPE24k->SetParameter(2, -2.07526e-30/0.0654841);

   TF1 *fApeVsPE = new TF1("fApeVsPE", "pol3", 0.0, 0.5e9);
   fApeVsPE->SetParameter(0, 1.);
   fApeVsPE->SetParameter(1, -4.95563e-09/4.99164);
   fApeVsPE->SetParameter(2, 4.93872e-18/4.99164);
   fApeVsPE->SetParameter(2, -2.82159e-27/4.99164);

   TF1 *fApeVsPE24k = new TF1("fApeVsPE24k", "pol3", 0.0, 0.5e9);
   fApeVsPE24k->SetParameter(0, 1.);
   fApeVsPE24k->SetParameter(1, -2.36757e-10);
   fApeVsPE24k->SetParameter(2, 6.32125e-20);
   fApeVsPE24k->SetParameter(2, -1.29205e-29);

   TF1 *fPDEVsPE = new TF1("fPDEVsPE", "pol3", 0.0, 0.5e9);
   fPDEVsPE->SetParameter(0, 0.999352);
   fPDEVsPE->SetParameter(1, -5.483e-10);
   fPDEVsPE->SetParameter(2, 3.28949e-19);
   fPDEVsPE->SetParameter(2, -1.42835e-28);

   TF1 *fPDEVsPE24k = new TF1("fPDEVsPE24k", "pol3", 0.0, 0.5e9);
   fPDEVsPE24k->SetParameter(0, 1.00005);
   fPDEVsPE24k->SetParameter(1, -1.2769e-10);
   fPDEVsPE24k->SetParameter(2, 1.92257e-20);
   fPDEVsPE24k->SetParameter(2, -3.23035e-30);

   TCanvas *cRelativeDrop = new TCanvas("cRelativeDrop", "cRelativeDrop", 800., 800.);
   fPxtVsPE->Draw();
   fApeVsPE->SetLineColor(8);
   fApeVsPE->Draw("same");
   fPDEVsPE->SetLineColor(4);
   fPDEVsPE->Draw("same");

   TLegend *leg = new TLegend(0.1,0.7,0.48,0.9);
   leg->SetHeader("R_{bias} = 10 k#Omega", "C");
   leg->AddEntry(fPxtVsPE,"P_{XT}","l");
   leg->AddEntry(fApeVsPE,"1 p.e. Amplitude","l");
   leg->AddEntry(fPDEVsPE,"PDE @ 470 nm.","l");
   leg->Draw();

   //TCanvas *cRelativeDrop24k = new TCanvas("cRelativeDrop24k", "", 800., 800.);
   fPxtVsPE24k->SetLineStyle(2);
   fPxtVsPE24k->Draw("same");
   fApeVsPE24k->SetLineColor(8);
   fApeVsPE24k->SetLineStyle(2);
   fApeVsPE24k->Draw("same");
   fPDEVsPE24k->SetLineColor(4);
   fPDEVsPE24k->SetLineStyle(2);
   fPDEVsPE24k->Draw("same");

   TLegend *leg24k = new TLegend(0.1,0.7,0.48,0.9);
   leg24k->SetHeader("R_{bias} = 2.4 k#Omega", "C");
   leg24k->AddEntry(fPxtVsPE24k,"P_{XT}","l");
   leg24k->AddEntry(fApeVsPE24k,"1 p.e. Amplitude","l");
   leg24k->AddEntry(fPDEVsPE24k,"PDE @ 470 nm.","l");
   leg24k->Draw();

   TF1 *f1ArelVsBaselineShift = new TF1("f1ArelVsBaselineShift", "pol2", 0.0, 80.);
   Double_t ArelVsBLPar[3];
     ArelVsBLPar[0] = 1.;
     ArelVsBLPar[1] = -0.00485225;
     ArelVsBLPar[2] = 2.71896e-07;
   f1ArelVsBaselineShift->SetParameter(0, ArelVsBLPar[0]);
   f1ArelVsBaselineShift->SetParameter(1, ArelVsBLPar[1]);
   f1ArelVsBaselineShift->SetParameter(2, ArelVsBLPar[2]);

   TF1 *f1ArelVsBaselineShift24k = new TF1("f1ArelVsBaselineShift24k", "pol2", 0.0, 80.);
   Double_t ArelVsBLPar24k[3];
     ArelVsBLPar24k[0] = 1.;
     ArelVsBLPar24k[1] = -0.00115421;
     ArelVsBLPar24k[2] = -2.99814e-08;
   f1ArelVsBaselineShift24k->SetParameter(0, ArelVsBLPar24k[0]);
   f1ArelVsBaselineShift24k->SetParameter(1, ArelVsBLPar24k[1]);
   f1ArelVsBaselineShift24k->SetParameter(2, ArelVsBLPar24k[2]);

   TF1 *f1PxtrelVsBaselineShift = new TF1("f1PxtrelVsBaselineShift", "pol2", 0.0, 80.);
   Double_t PxtRelVsBLPar[3];
     PxtRelVsBLPar[0] = 1.;
     PxtRelVsBLPar[1] = -0.0088091;
     PxtRelVsBLPar[2] = 1.82857e-05;
   f1PxtrelVsBaselineShift->SetParameter(0, PxtRelVsBLPar[0]);
   f1PxtrelVsBaselineShift->SetParameter(1, PxtRelVsBLPar[1]);
   f1PxtrelVsBaselineShift->SetParameter(2, PxtRelVsBLPar[2]);

   TF1 *f1PxtrelVsBaselineShift24k = new TF1("f1PxtrelVsBaselineShift24k", "pol2", 0.0, 80.);
   Double_t PxtRelVsBLPar24k[3];
     PxtRelVsBLPar24k[0] = 1.;
     PxtRelVsBLPar24k[1] = -0.00208218;
     PxtRelVsBLPar24k[2] = 6.87533e-07;
   f1PxtrelVsBaselineShift24k->SetParameter(0, PxtRelVsBLPar24k[0]);
   f1PxtrelVsBaselineShift24k->SetParameter(1, PxtRelVsBLPar24k[1]);
   f1PxtrelVsBaselineShift24k->SetParameter(2, PxtRelVsBLPar24k[2]);


   TF1 *f1PDErelVsBaselineShift = new TF1("f1PDErelVsBaselineShift", "pol2", 0.0, 80.);
   Double_t PDERelVsBLPar[3];
     PDERelVsBLPar[0] = 1.;
     PDERelVsBLPar[1] = -0.00258286;
     PDERelVsBLPar[2] = -9.07788e-06;
   f1PDErelVsBaselineShift->SetParameter(0, PDERelVsBLPar[0]);
   f1PDErelVsBaselineShift->SetParameter(1, PDERelVsBLPar[1]);
   f1PDErelVsBaselineShift->SetParameter(2, PDERelVsBLPar[2]);

   TF1 *f1PDErelVsBaselineShift24k = new TF1("f1PDErelVsBaselineShift24k", "pol2", 0.0, 80.);
   Double_t PDERelVsBLPar24k[3];
     PDERelVsBLPar24k[0] = 1.;
     PDERelVsBLPar24k[1] = -0.00061547;
     PDERelVsBLPar24k[2] = -5.09818e-07;
   f1PDErelVsBaselineShift24k->SetParameter(0, PDERelVsBLPar24k[0]);
   f1PDErelVsBaselineShift24k->SetParameter(1, PDERelVsBLPar24k[1]);
   f1PDErelVsBaselineShift24k->SetParameter(2, PDERelVsBLPar24k[2]);

   //f1NSBvsBaselineShift->Eval(BaselineShift)

   //grAmpliDrop[2]->GetYaxis()->SetRangeUser(0.95*AEnd, 1.05*AStart);
   //grAmpliDrop[2]->GetXaxis()->SetRangeUser(190, 325);
   //c1MainPresentation->cd(3)->Update();

   TCanvas *c1RelDropVsBaseline = new TCanvas("c1RelDropVsBaseline", "c1RelDropVsBaseline", 800., 800.);
   c1RelDropVsBaseline->SetGrid();
   Double_t X2P[2], Y2P[2];
   X2P[0] = 0.0; X2P[1] = 80.;
   Y2P[0] = 0.4; Y2P[1] = 1.05;

   TGraph *gr2Paxis = new TGraph(2, X2P, Y2P);
   gr2Paxis->SetTitle("");
   gr2Paxis->GetXaxis()->SetTitle("Baseline Shift, ADC");
   gr2Paxis->GetYaxis()->SetTitle("Relative Drop");
   gr2Paxis->Draw("AP");
   gr2Paxis->GetYaxis()->SetRangeUser(Y2P[0], Y2P[1]);
   gr2Paxis->GetXaxis()->SetRangeUser(X2P[0], X2P[1]);
   c1RelDropVsBaseline->Update();

   f1PxtrelVsBaselineShift->SetLineColor(2);
   f1PxtrelVsBaselineShift->Draw("same");
   f1PDErelVsBaselineShift->SetLineColor(4);
   f1PDErelVsBaselineShift->Draw("same");
   f1ArelVsBaselineShift->SetLineColor(8);
   f1ArelVsBaselineShift->Draw("same");

   TLegend *leg10k = new TLegend(0.1,0.7,0.48,0.9);
   leg10k->SetHeader("R_{bias} = 10 k#Omega", "C");
   leg10k->AddEntry(f1PxtrelVsBaselineShift,"P_{XT}","l");
   leg10k->AddEntry(f1ArelVsBaselineShift,"1 p.e. Amplitude","l");
   leg10k->AddEntry(f1PDErelVsBaselineShift,"PDE @ 470 nm.","l");
   leg10k->Draw();

   f1PxtrelVsBaselineShift24k->SetLineColor(2);
   f1PxtrelVsBaselineShift24k->SetLineStyle(10);
   f1PxtrelVsBaselineShift24k->Draw("same");
   f1PDErelVsBaselineShift24k->SetLineColor(4);
   f1PDErelVsBaselineShift24k->SetLineStyle(10);
   f1PDErelVsBaselineShift24k->Draw("same");
   f1ArelVsBaselineShift24k->SetLineColor(8);
   f1ArelVsBaselineShift24k->SetLineStyle(10);
   f1ArelVsBaselineShift24k->Draw("same");

   TLegend *leg24kbl = new TLegend(0.1,0.7,0.48,0.9);
   leg24kbl->SetHeader("R_{bias} = 2.4 k#Omega", "C");
   leg24kbl->AddEntry(f1PxtrelVsBaselineShift24k,"P_{XT}","l");
   leg24kbl->AddEntry(f1ArelVsBaselineShift24k,"1 p.e. Amplitude","l");
   leg24kbl->AddEntry(f1PDErelVsBaselineShift24k,"PDE @ 470 nm.","l");
   leg24kbl->Draw();

   //TGaxis *axis1 = new TGaxis(X2P[0], Y2P[1], X2P[1], Y2P[1], 0.0, f1NSBvsBaselineShift->Eval(X2P[1]), 50510, "-");
   TF1 *fBaselineVsNSB = new TF1("fBaselineVsNSB", "pol2", 0.0, 100.);
   fBaselineVsNSB->SetParameter(0, 0.0);
   fBaselineVsNSB->SetParameter(1, 4.24567e+06);
   fBaselineVsNSB->SetParameter(2, 54281.3);

   TGaxis *axis1 = new TGaxis(X2P[0], Y2P[1], X2P[1], Y2P[1], X2P[0], X2P[1], 50510, "-");

   axis1->ChangeLabel(1,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(0.)/1.e6));
   axis1->ChangeLabel(2,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(10.)/1.e6));
   axis1->ChangeLabel(3,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(20.)/1.e6));
   axis1->ChangeLabel(4,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(30.)/1.e6));
   axis1->ChangeLabel(5,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(40.)/1.e6));
   axis1->ChangeLabel(6,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(50.)/1.e6));
   axis1->ChangeLabel(7,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(60.)/1.e6));
   axis1->ChangeLabel(8,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(70.)/1.e6));
   axis1->ChangeLabel(9,-1,-1,-1, 1,-1, Form("%3.0f", fBaselineVsNSB->Eval(80.)/1.e6));
  //TGaxis *A2 = new TGaxis(1,1,9,1,"f2");
   axis1->SetTitle("NSB, MHz (p.e.)");
   axis1->SetLabelSize(0.04);
   axis1->SetTitleSize(0.04);
   axis1->Draw();
   
   TCanvas *c1BaselineVsNSB = new TCanvas("c1BaselineVsNSB", "c1BaselineVsNSB", 800, 800);
   fBaselineVsNSB->Draw();
   
   TF1 *fBaselineVsNSBYves = new TF1("fBaselineVsNSBYves", "pol3", 0.0, 100.);
   fBaselineVsNSBYves->SetParameter(0, -2.4464*1e6);;
   fBaselineVsNSBYves->SetParameter(1, 1.20598*1e7);
   fBaselineVsNSBYves->SetParameter(2, 7.3408*1e4);
   fBaselineVsNSBYves->SetParameter(3, -5.77111);
   fBaselineVsNSBYves->SetLineColor(4);
   fBaselineVsNSBYves->Draw("same");

}
